<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - ' . Yii::t('app', 'Price');
$this->breadcrumbs=array(
	Yii::t('app', 'Price'),
);
?>
<h5 class="title">Báo giá gói dịch vụ:</h5>
<div>
	<div class="packet">
	    <span class="label">BIZ 10</span>
	    <ul>
	      <li class="tooltip-holder"><strong>10.000 </strong> Email </li>
	      <li><strong>350.000 đ</strong></li>
	      <li><strong>35 đ/email</strong></li>
	    </ul>
	</div>
	<div class="packet">
	    <span class="label">BIZ 50</span>
	    <ul>
	      <li class="tooltip-holder"><strong>50.000 </strong> Email</li>
	      <li><strong>1.000.000 đ</strong></li>
	      <li><strong>20 đ/email</strong></li>
	    </ul>
	</div>
	<div class="packet">
	    <span class="label">BIZ 100</span>
	    <ul>
	      <li class="tooltip-holder"><strong>100.000 </strong> Email</li>
	      <li><strong>1.800.000 đ</strong></li>
	      <li><strong>18 đ/email</strong></li>
	    </ul>
	</div>
	<div class="packet">
	    <span class="label">BIZ 500</span>
	    <ul>
	      <li><strong>500.000 </strong>  Email </li>
	      <li><strong>4.000.000 đ</strong></li>
	      <li><strong>8 đ/email</strong></li>
	    </ul>
	</div>
	<div class="packet">
		<span class="label">BIZ 1000</span>
		<ul>
		  <li><strong>1.000.000 </strong> Email
		  </li>
		  <li><strong>7.000.000 đ</strong></li>
		  <li><strong>7 đ/email</strong></li>
		</ul>
	</div>
</div>
<div style="float:left;width:100%;">
<b>Ghi chú:</b>
<ul style="padding:0 10px;">
	<li>Bảng giá áp dụng cho doanh nghiệp đã có dữ liệu(data) người dùng sẵn.</li>
	<li>Tốc độ gửi email tối thiểu cam kết: <b>3.000 email/h</b></li>
	<li>Trong trường hợp quí khách gửi có số email hỏng (bounce)  đạt mức trên 5000 trong một tháng &amp; tỷ lệ email hỏng/email đã gửi &gt; 10% (bounce rate &gt;10%) sẽ không thể tiếp tục gửi email trong tháng.</li>
</ul>
</div>
<h5 class="title">Phương thức thanh toán:</h5>
	<div>
		<ul style="padding:0 10px;">
			<li>Bạn thanh toán gói dịch vụ bằng cách chuyển tiền vào tài khoản bên dưới (Nội dung chuyển tiền ghi rõ tên đăng nhập và gói dịch vụ).</li>
			<li>Sau khi nhận được thông báo chuyển tiền chúng tôi sẽ kích hoạt dịch vụ.</li>
		</ul>
		<hr />
		<table width="100%">
			<tr>
				<td width="50%">
					<b>Ngân hàng techcombank - Chi nhánh Hoàng Quốc Việt</b> <br />
					Chủ tài khoản: Trần Văn Kiên <br />
					Số tài khoản: 11721641802019 <br />
				</td>
				<td width="50%">
					<b>Ngân hàng Quân đội (MB) - Chi nhánh Tây đô</b> <br />

					Chủ tài khoản: Trần Văn Kiên<br />
					Số tài khoản: 0590102977008<br />
				</td>
			</tr>
		</table>
	</div>
<div>
</div>