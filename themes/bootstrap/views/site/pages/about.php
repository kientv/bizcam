<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - ' . Yii::t('app', 'About');
$this->breadcrumbs=array(
	Yii::t('app', 'About'),
);
?>
<p>
Với sự phát triển mạnh mẽ của Internet và thương mại điện tử, ngày nay người ta đã tận dụng các ưu điểm của email 
để ứng dụng vào công việc kinh doanh trên Internet và hình thức này đã mang lại hiệu quả khá tốt.<br><br>
Vậy hình thức marketing bằng email là gì? Tại sao lại hiệu quả như vậy? Marketing bằng email là một hình thức 
mà người làm marketing sử dụng email, sách điện tử hay catalogue điện tử để gửi đến cho khách hàng, thúc đẩy 
và đưa khách hàng đến quyết định thực hiện việc mua các sản phẩm của họ.<br><br>
<strong>Hoạt động marketing bằng email gồm 2 hình thức:</strong><br><br>
• Email marketing cho phép hay được sự cho phép của người nhận (Solicited Commercial Email), 
đây là hình thức hiệu quả nhất.<br><br>
• Email marketing không được sự cho phép của người nhận (Unsolicited Email Marketing hay Unsolicited Commercial Email - UCE) 
còn gọi là Spam. Đây là hai hình thức marketing bằng email đầu tiên xuất hiện trên Internet.<br><br>
Đối với các công ty lớn, nhất là các công ty hoạt động trong lĩnh vực thương mại điện tử, 
sử dụng email để chăm sóc khách hàng là một đòi hỏi tất yếu. Tuy nhiên, để có một hệ thống gửi email 
tốt đòi hỏi giải pháp tổng thể cả về phần cứng và phần mềm.<br><br>
BIZCAM cung cấp giải pháp tối ưu cho doanh nghiệp thường xuyên gửi bản tin, 
thông báo, quảng cáo chăm sóc khách hàng với số lượng lớn. 
Với chi phí phù hợp với các DN Việt Nam,<br><br>
BIZCAM là phần mềm email marketing trực tuyến có nhiều tính năng ưu việt.<br><br>
<em>Những lý do đáng để các Doanh nghiệp quan tâm và sử dụng dịch vụ này:</em><br><br>
- Giải pháp tổng thể cho việc chăm sóc khách hàng qua email. <br><br>
- Chất lượng dịch vụ không thua kém nước ngoài nhưng với chi phí chỉ bằng 1/3.<br><br>
- Phần mềm trực tuyến nhiều tính năng ưu việt: cho phép quản lý email list, thiết kế mẫu email, gửi email theo lịch, 
thống kê kết quả các đợt gửi... chỉ với một tài khoản duy nhất. <br><br>
- Liên kết với các Email Provider như Gmail, Yahoo! Mail, Hotmail,... 
nhằm đảm bảo tỷ lệ gửi nhận email thành công cao nhất. <br><br>
- Hỗ trợ tư vấn cho doanh nghiệp thiết đặt hệ thống chăm sóc khách hàng trực tuyến, tạo nội dung và thiết kế mẫu email chuyên nghiệp. <br><br>
<strong>bizcam.vn tuân thủ chặt chẽ các Nghị định về chống thư rác:</strong><br><br>
Các DN sử dụng bizcam.vn EM chủ yếu gửi email cho các khách hàng của họ chủ động đăng ký nhận tin hoặc đã đồng ý nhận, 
tuy nhiên để tôn trọng người nhận email &amp; thể hiện văn hóa đúng mực, 
chúng tôi vẫn bắt buộc các công ty khách hàng phải tuân thủ các quy định của 
Pháp luật về chống thư rác khi sử dụng phần mềm của bizcam.vn EM.<br><br>
( Giấy phép VNCert )<br><br><strong>Đầu tư máy chủ &amp; phần mềm chuyên nghiệp:</strong><br><br>
Khách hàng có thể gửi tới hàng triệu email mỗi tháng. Đảm bảo tỷ lệ gửi email thành công cao nhất. 
Quản lý chiến dịch dễ dàng, chuyên nghiệp &amp; nhiều tính năng.<br><br>
Hướng dẫn sử dụng dịch vụ Email Marketing ( Download )<br>Một số chiến dịch Email thành công ( Download )
</p>
<!--
<?php $this->widget('bootstrap.widgets.TbLabel', array(
    'type'=>'success', // 'success', 'warning', 'important', 'info' or 'inverse'
    'label'=>'Success',
)); ?> 
<hr />
<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'inverse',
    'toggle' => 'radio', // 'checkbox' or 'radio'
    'buttons' => array(
        array('label'=>'Left'),
        array('label'=>'Middle'),
        array('label'=>'Right'),
    ),
)); ?>
<hr />
<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
)); ?>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Submit')); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'Reset')); ?>
</div>
<?php $this->endWidget(); ?>
 -->