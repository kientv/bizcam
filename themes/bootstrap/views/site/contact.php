<?php
$this->pageTitle=Yii::app()->name . ' - ' . Yii::t('app','Contact');
$this->breadcrumbs=array(
	Yii::t('app','Contact'),
);
?>

<h5><?php echo Yii::t('app','Contact information')?>:</h5>
<hr />
<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>
<table width="100%">
	<tr>
		<td width="30%" valign="top">
			<div class="radius">
				<b>BIZCAM.VN team</b> <br /> <br />
				Email: kientranvan@gmail.com <br />
				Hotline: 094.291.8384 <br />
				Website: bizcam.vn
			</div>
		</td>
		<td>
				<div class="form">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'contact-form',
					'enableClientValidation'=>true,
					'clientOptions'=>array(
						'validateOnSubmit'=>false,
					),
				)); ?>
				
					<?php
					if($model->hasErrors()) {
						Yii::app()->user->setFlash('error', $form->errorSummary($model));
						$this->widget('bootstrap.widgets.TbAlert', array(
								'block'=>true, // display a larger alert block?
								'fade'=>true, // use transitions?
								'closeText'=>'×', // close link text - if set to false, no close link is displayed
								'alerts'=>array( // configurations per alert type
								'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'), // success, info, warning, error or danger
							),
						)); 
					}
					?>
				<table border="0" width="50%">
					<tr>	
						<td>
							<div>
								<?php echo Yii::t('app','Name')?> * <br />
								<?php echo $form->textField($model,'name'); ?>
								<?php echo $form->error($model,'name'); ?>
							</div>
						
							<div>
								<?php echo Yii::t('app','Email')?> * <br />
								<?php echo $form->textField($model,'email'); ?>
							</div>
						
							<div>
								<?php echo Yii::t('app','Subject')?> * <br />
								<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
							</div>	
						</td>
						<td valign="top">
							<?php echo Yii::t('app','Body')?> * <br />
							<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>150)); ?>
						</td>
					</tr>
				</table>
				<?php if(CCaptcha::checkRequirements()): ?>
				<div>
					<?php echo Yii::t('app','Verification Code')?> * <br />
					<div>
					<?php $this->widget('CCaptcha'); ?>
					<?php echo $form->textField($model,'verifyCode'); ?>
					</div>
					<div class="hint">
						Vui lòng nhập mã xác thực như ảnh bên <i>(Không phân biệt hoa, thường).</i>
					</div>
				</div>
				<?php endif; ?>
				
					<div class="buttons">
						<?php
						$this->widget('bootstrap.widgets.TbButton',array(
							'label' => Yii::t('app','Send'),
							'buttonType'=>'submit',
							'size' => 'small'
							)); 
						?>
					</div>
				
				<?php $this->endWidget(); ?>
				
				</div><!-- form -->
		</td>
	</tr>
</table> 
<hr />
<?php endif; ?>