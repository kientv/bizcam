<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<?php $this->widget('bootstrap.widgets.TbCarousel', array(
    'items'=>array(
        array('image'=>Yii::app()->baseUrl . '/images/em00.png'),
		array('image'=>Yii::app()->baseUrl . '/images/em01.jpg'),
        array('image'=>Yii::app()->baseUrl . '/images/em02.jpg'),
        array('image'=>Yii::app()->baseUrl . '/images/em03.jpg'),
    ),
)); ?>
<div class="packet">
    <span class="label">BIZ 10</span>
    <ul>
      <li class="tooltip-holder"><strong>10.000 </strong> Email </li>
      <li><strong>350.000 đ</strong></li>
      <li><strong>35 đ/email</strong></li>
    </ul>
</div>
<div class="packet">
    <span class="label">BIZ 50</span>
    <ul>
      <li class="tooltip-holder"><strong>50.000 </strong> Email</li>
      <li><strong>1.000.000 đ</strong></li>
      <li><strong>20 đ/email</strong></li>
    </ul>
</div>
<div class="packet">
    <span class="label">BIZ 100</span>
    <ul>
      <li class="tooltip-holder"><strong>100.000 </strong> Email</li>
      <li><strong>1.800.000 đ</strong></li>
      <li><strong>18 đ/email</strong></li>
    </ul>
</div>
<div class="packet">
    <span class="label">BIZ 500</span>
    <ul>
      <li><strong>500.000 </strong>  Email </li>
      <li><strong>4.000.000 đ</strong></li>
      <li><strong>8 đ/email</strong></li>
    </ul>
</div>
<div class="packet">
	<span class="label">BIZ 1000</span>
	<ul>
	  <li><strong>1.000.000 </strong> Email
	  </li>
	  <li><strong>7.000.000 đ</strong></li>
	  <li><strong>7 đ/email</strong></li>
	</ul>
</div>
<div class="partners">
	<!--     
	<a style="background-image: url(http://www.matbao.net/getattachment/06de21c3-c795-48b3-915c-53159a4fc31c//Đoi-tac/Thanh-Toan/SmartLink.aspx?width=72&height=82);width:72px;" title="SmartLink">
	     &nbsp;
	</a> 
	 -->       
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/techcombank.png);width:86px;" title="TECHCOMBANK"></a>        
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/vietinbank.png);width:130px;" title="VietinBank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/vib.png);width:86px;" title="VIB"></a>        
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/mb.png);width:86px;" title="MB Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/eximbank.png);width:163px;" title="EXIMBANK"></a>        
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/maritimebank.png);width:119px;" title="MARITIMEBANK"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/sacombank.png);width:150px;" title="Sacombank"></a>        
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/vietabank.png);width:61px;" title="VIETABANK"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/hdbank.png);width:115px;" title="HD Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/oceanbank.png);width:125px;" title="Ocean Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/gpbank.png);width:158px;" title="GPBank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/acbbank.png);width:86px;" title="ACB Bank"></a>        
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/navibank.png);width:169px;" title="Navi Bank"></a>        
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/vpbank.png);width:119px;" title="VP Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/abbank.png);width:142px;" title="AB Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/bacabank.png);width:125px;" title="BacA Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/daiabank.png);width:141px;" title="DaiA Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/dongabank.png);width:84px;" title="DongA Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/phuongdongbank.png);width:84px;" title="PhuongDong Bank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/vietcombank.png);width:112px;" title="Vietcombank"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/paypal.png);width:101px;" title="PayPal"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/mastercard.png);width:65px;" title="MasterCard"></a>
	<a style="background-image: url(<?php echo Yii::app()->baseUrl?>/images/visacard.png);width:65px;" title="Visa Card">
	     &nbsp;
	</a>
</div>