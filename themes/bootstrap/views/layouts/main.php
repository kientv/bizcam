<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
	
	<!--Start of Zopim Live Chat Script-->

	<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                //array('label'=>Yii::t('app', 'Home'), 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>Yii::t('app', 'About'), 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>Yii::t('app', 'Price'), 'url'=>array('/site/page', 'view'=>'price')),
                array('label'=>Yii::t('app', 'News'), 'url'=>array('/store/store'),),
                array('label'=>Yii::t('app', 'Contact'), 'url'=>array('/site/contact')),
                array('label'=>Yii::t('app', 'Login'), 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>Yii::t('app', 'Register'), 'url'=>array('/user/registration'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>Yii::t('app', 'Logout') . ' ('.Yii::app()->user->name.')', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container" id="page">
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>
	<div class="clear"></div>
	<div class="footer">
		Copyright &copy; <?php echo date('Y'); ?> by 
		<?php $this->widget('bootstrap.widgets.TbLabel', array(
			'type'=>'info', // 'success', 'warning', 'important', 'info' or 'inverse'
			'label'=>'bizcam.vn team',
		)); ?>
		All Rights Reserved.<br/>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->
</div><!-- page -->
</body>
</html>
