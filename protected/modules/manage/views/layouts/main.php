<?php
//	Yii::app()->clientscript
//		->registerCssFile( $this->module->getAssetsUrl() . '/css/bootstrap.css' )
//		->registerCssFile( $this->module->getAssetsUrl() . '/css/bootstrap-responsive.css' )
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $this->pageTitle; ?></title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le styles -->
	<style>
		body {
			padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
		}

		@media (max-width: 980px) {
			body {
				padding-top: 0px;
			}
		}
	</style>

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico">
	<!--Uncomment when required-->
	<!--<link rel="apple-touch-icon" href="images/apple-touch-icon.png">-->
	<!--<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">-->
	<!--<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">-->
</head>

<body>
<?php $this->widget('bootstrap.widgets.TbNavbar', array(
	'brand' => '<span class="label label-important">' . Yii::app()->name . '</span>',
	'brandUrl' => 'http://' . Yii::app()->name . '/index.php/manage',
	'collapse' => true,
	'items' => array(
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'items' => array(
				/** change this link according to your backend dashboard **/
				array('label' =>  ManageModule::t('Email list'), 'url' => '#', 'items' => array(
					array('label' =>  ManageModule::t('List'), 'url' => array('/manage/emaillist')),
					array('label' =>  ManageModule::t('Create'), 'url' => array('/manage/emaillist/create')),
				)),
				array('label' =>  ManageModule::t('Campaigns'), 'url' => '#', 'items' => array(
					array('label' =>  ManageModule::t('Manage'), 'url' => array('campaign/manage')),
					array('label' =>  ManageModule::t('Create'), 'url' => array('campaign/create')),
					'',
					array('label' =>  ManageModule::t('Stats'), 'url' => array('campaign/stats')),
				)),
				array('label' =>  ManageModule::t('Template'), 'url' => '#', 'items' => array(
					array('label' =>  ManageModule::t('Manage template'), 'url' => array('template/')),	
					array('label' =>  ManageModule::t('Create template'), 'url' => array('template/create'))	
				)),
				array('label' =>  ManageModule::t('Stats'), 'url' => array('amazon/manage'), 'visible' => Yii::app()->user->checkAccess('admin')),
				array('label' =>  ManageModule::t('Unsubscribed'), 'url' => array('unsubscribed/manage')),
				array('label' =>  ManageModule::t('Login'), 'url' => array('/site/login'), 'visible' => $this->module->useOwnLogin && Yii::app()->user->isGuest),
				array('label' =>  ManageModule::t('Logout') . ' (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => $this->module->useOwnLogin &&  !Yii::app()->user->isGuest)
			)
		)
	),
)); ?>

<!-- / starts container -->
<div class="container">
	<?php echo $content ?>
</div>
<!-- /container -->

<hr/>

<!-- /footer -->
<footer>
	<div class="container">
	</div>
</footer>
<!-- /footer -->

</body>
</html>
