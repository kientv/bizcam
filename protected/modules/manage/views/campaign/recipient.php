<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'campaign-users-grid',
	'dataProvider' => $dataProvider,
	//'filter' => $to_address,
	'type' => 'striped bordered condensed',
	'summaryText'=>false,
	'afterAjaxUpdate'=>'function(id, data){
		$("#"+id).find("input[type=\'checkbox\']").on("click",campaign.selectRecipient);
	}',
	'columns' => array(
		array(
			'header'=> ManageModule::t('To Name'),
			'name'=>'to_name',
		),
		array(
			'header'=> ManageModule::t('To Address'),
			'name'=>'to_address',
		),
	),
));
?>