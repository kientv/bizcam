<?php
//@var $this RecipientController 
$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
	'homeLink' => false,
	'links' => array(
		ManageModule::t('Recipient'),
	),
	'separator' => '<span class="divider">/</span>',
	'htmlOptions' => array('class' => 'breadcrumb')
));
?>
<h4><?php echo $campaignName; ?></h4>
<hr />
<?php $this->widget('bootstrap.widgets.TbButton', array(
	'buttonType' => 'link',
	'label' => ManageModule::t('Import CSV'),
	'url' => $this->createUrl('recipient/import', array('id' => $_REQUEST['id'])),
	'htmlOptions' => array('class' => 'btn secondary', 'data-modal-width' => '820px', 'id' => 'import')));
?>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'campaign-users-grid',
	'dataProvider' => $dataProvider,
	//'filter' => $to_address,
	'type' => 'striped bordered condensed',
	'summaryText'=>false,
	'afterAjaxUpdate'=>'function(id, data){
		$("#"+id).find("input[type=\'checkbox\']").on("click",campaign.selectRecipient);
	}',
	'columns' => array(
		array(
			'header'=> ManageModule::t('To Name'),
			'name'=>'to_name',
		),
		array(
			'header'=> ManageModule::t('To Address'),
			'name'=>'to_address',
		),
	),
));

$assetsUrl = $this->module->assetsUrl;
Yii::app()->clientScript->registerScriptFile($assetsUrl . '/js/jquery.popups.js', CClientScript::POS_END);
?>
<script language="javascript">
$("#import").on("click",function(){
	var w = $(this).data("modal-width"), iw = w.substring(0,w.length-2);
	simplePrompt({
		headerHtml: "<h3>Import CSV</h3>",
		messageHtml: "<iframe src=\""+this.href+"\" width=\""+(iw-20)+"\" height=\"420\" style=\"margin:5px;border:0\"/>",
		width: w,
		height: "540px",
		buttons:[{ label: "Ok", "default": true}]
	});
	return false;
});
if(!$("#modal").length)
{
	$modal = $("<div/>").prop("id","modal");
	$("body").append($modal);
}
</script>