<?php
#css
//ob_start();
?>
<?php
/*
$css = ob_get_contents();
ob_end_clean();

#markup
ob_start();
*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Acme Corporation</title>
</head>
<body style="background-color: #fafafa;" marginheight="0" topmargin="0" marginwidth="0" bgcolor="#fafafa"
      leftmargin="0">
<table width="100%" border="0">
	<tr>
		<td align="left"><img src="http://placehold.it/200x200" alt="" border="0"/></td>
	</tr>
	<tr>
		<td>
			Dear, <br />
			<?php echo $content;?>
			<br />
			<i>thanks & regards</i><br />
			bizcam.vn team
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<p class="small_text">
				You are receiving this email because you signed up for {{COMPANY NAME}}. If you believe this has been sent to you in error or don't want to continue receiving emails from us, please safely <a href="<?php echo $unsubscribeUrl?>" title="" class="unsubscribe_link">unsubscribe</a>.
			</p>
		</td>
	</tr>
</table>
<img src="--IMGSTATS--" width="1" height="1"/>
<?php
/*
$markup = ob_get_contents();
ob_end_clean();

$email = new CSSToInlineStylesHelper($markup, $css);
echo $email->convert();
*/
?>