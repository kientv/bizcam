<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Acme Corporation</title>
</head>
<body style="background-color: #fafafa;" marginheight="0" topmargin="0" marginwidth="0" bgcolor="#fafafa" leftmargin="0">
<table width="100%" border="0">
	<tr>
		<td width="42%" align="left"><img src="http://placehold.it/200x200" alt="" border="0"/></td>
		<td width="58%" align="right">
			<table border="0" cellpadding="5">
				<tr>
					<td align="center">
						<a href="https://twitter.com/" title="Twitter"><img src="http://placehold.it/21x21" alt="" width="21" border="0"/></a>
					</td>
					<td align="center">
						<a href="http://www.facebook.com" title="Facebook"><img src="http://placehold.it/21x21" alt="" width="21" border="0"/></a>
					</td>
					<td align="center">
						<a href="http://pinterest.com/" title=Pinterest"><img src="http://placehold.it/21x21" alt="" width="21" border="0" /></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<?php echo $content;?>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<p class="small_text">
				You are receiving this email because you signed up for {{COMPANY NAME}}. If you believe this has been sent to you in error or don't want to continue receiving emails from us, please safely <a href="<?php echo $unsubscribeUrl?>" title="" class="unsubscribe_link">unsubscribe</a>.
			</p>
		</td>
	</tr>
	<tr>		
		<td colspan="2" id="footer_container">
			<table id="footer" border="0" cellspacing="5">
				<tr>
					<td>{{COMPANY NAME}}</td>
					<td align="center" valign="middle"> | </td>
					<td>{{COMPANY ADDRES}}</td>
					<td align="center" valign="middle"> | </td>
					<td>{{COMPANY CITY}}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>