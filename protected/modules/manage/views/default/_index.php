<?php
$this->breadcrumbs = array(
	$this->module->id,
	'Packages'
);
?>
<div class="alert alert-info">
	<a class="close" data-dismiss="alert" href="#">×</a>
	<h4 class="alert-heading">BIZCAM EMAIL MARKETING</h4>
	<?php echo Yii::t('app','we always improve and enhance quality of service');?>
</div>
<?php
$this->beginWidget('bootstrap.widgets.TbHeroUnit', array(
	'heading' =>  Yii::t('app','Amazon SES Bulk Mailer'),
)); ?>

<p><?php echo  Yii::t('app','Send email easy.');?></p>
<p><?php $this->widget('bootstrap.widgets.TbButton', array(
	'type' => 'primary',
	'size' => 'large',
	'label' =>  Yii::t('app','Get Started'),
	'url' => array('campaign/manage')
)); ?></p>

<?php $this->endWidget(); ?>
