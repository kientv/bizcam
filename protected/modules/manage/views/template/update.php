<?php
$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
	'homeLink' => false,
	'links' => array(
		'Template'=>array('view','id'=>$model->id),
		'Update template',
	),
	'separator' => '<span class="divider">/</span>',
	'htmlOptions' => array('class' => 'breadcrumb')
));
?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>