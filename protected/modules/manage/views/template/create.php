<?php
$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
	'homeLink' => false,
	'links' => array(
		'Create template',
	),
	'separator' => '<span class="divider">/</span>',
	'htmlOptions' => array('class' => 'breadcrumb')
));
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>