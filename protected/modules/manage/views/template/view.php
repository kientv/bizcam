<?php
$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
	'homeLink' => false,
	'links' => array(
		$model->name,
	),
	'separator' => '<span class="divider">/</span>',
	'htmlOptions' => array('class' => 'breadcrumb')
));
?>
<table width="100%" class="hero-unit" cellpadding="10px">
	<tr>
		<td>
			<span class="label label-important">
				<?php echo ManageModule::t('Description')?>
			</span>
			&nbsp;<?php echo $model->description?> <br />
			<span class="label label-important">
				<?php echo ManageModule::t('Variables')?>
			</span>
			&nbsp;<?php echo $model->variables?>
		</td>
		<td width="50%" align="right">
			<?php echo CHtml::image($this->module->assetsUrl . '/images/thumbnair/' . $model->image_src)?>
		</td>
	</tr>
</table>
<hr />
<fieldset>
<?php echo $model->html_body?>
</fieldset>