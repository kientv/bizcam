<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image_src')); ?>:</b>
	<?php echo CHtml::encode($data->image_src); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('html_body')); ?>:</b>
	<?php echo CHtml::encode($data->html_body); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_body')); ?>:</b>
	<?php echo CHtml::encode($data->text_body); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('variables')); ?>:</b>
	<?php echo CHtml::encode($data->variables); ?>
	<br />


</div>