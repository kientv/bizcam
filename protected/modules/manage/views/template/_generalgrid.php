<?php 
$this->widget('bootstrap.widgets.TbGridView',array(
		'type' => 'striped bordered condensed',
		'summaryText'=>false,
		'id'=>'template-grid',
		'dataProvider'=>$model,
		'columns'=>array(
			array(
				'header' => ManageModule::t('#'),
				'type'=>'raw',
				'value'=>'++$row',
				'htmlOptions'=>array('style'=>'text-align:center;')
			),
			array(
				'header' => ManageModule::t('Template'),
				'type'=>'raw',
				'value'=>'$data->name',
			),
			array(
				'header' => ManageModule::t('Description'),
				'type'=>'raw',
				'value'=>'$data->description',
			),
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
	   			'template'=>Yii::app()->user->checkAccess('admin')?'{view}{update}{delete}':'{view}',
			),
		),
	));
?>