<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'template-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

	<p class="help-block">
	<?php echo ManageModule::t('Fields with <span class="required">*</span> are required.')?>
	</p>

	<?php echo $form->errorSummary($model); ?>
	<span><?php echo ManageModule::t('Name')?></span>: <span class="required">*</span><br />
	<?php echo $form->textField($model,'name',array('class'=>'span5','maxlength'=>255)); ?>
	<br />
	<span><?php echo ManageModule::t('Description')?></span>: <br />
	<?php echo $form->textField($model,'description',array('class'=>'span5','maxlength'=>200)); ?>
	<br />
	<span><?php echo ManageModule::t('Variables')?></span>: <br />
	<?php echo $form->textField($model,'variables',array('class'=>'span5','maxlength'=>255)); ?>
	<br />
	<span><?php echo ManageModule::t('Thumbnair')?></span>:
	<span class="required">*</span> <br />
	<?php echo $form->fileField($model,'image_src',array('class'=>'span5')); ?>

	<?php $this->widget('application.extensions.fckeditor.FCKEditorWidget',array(
	    "model"=>$model,                # Data-Model
	    "attribute"=>'html_body',         # Attribute in the Data-Model
	    "height"=>'400px',
	    "width"=>'100%',
	    "toolbarSet"=>'Custom',          # EXISTING(!) Toolbar (see: fckeditor.js)
	    "fckeditor"=>Yii::app()->basePath."/../fckeditor/fckeditor.php",
	                                    # Path to fckeditor.php
	    "fckBasePath"=>Yii::app()->baseUrl."/fckeditor/",
	                                    # Realtive Path to the Editor (from Web-Root)
	    "config" => array("EditorAreaCSS"=>Yii::app()->baseUrl.'/css/main.css',),
	                                    # Additional Parameter (Can't configure a Toolbar dynamicly)
	) ); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? ManageModule::t('Create') : ManageModule::t('Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
