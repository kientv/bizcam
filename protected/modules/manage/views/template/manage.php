<?php
$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
	'homeLink' => false,
	'links' => array(
		ManageModule::t('Manage template'),
	),
	'separator' => '<span class="divider">/</span>',
	'htmlOptions' => array('class' => 'breadcrumb')
));
?>
<?php 
$this->widget('bootstrap.widgets.TbTabs', array(
	'type' => 'tabs',
	'tabs' => array(
		array(
			'label' => ManageModule::t('Your template'),
			'active' => true,
			'content' => $this->renderPartial('_usergrid', array('model'=>$model->search(Yii::app()->user->id)), true)
		),
		array(
			'label' => ManageModule::t('Genaral template'),
			'content' => $this->renderPartial('_generalgrid', array('model'=>$model->search(0)), true)
		)
	)
));
?>
