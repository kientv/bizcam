<?php
$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
	'homeLink' => false,
	'links' => array(
		'Emaillists',
	),
	'separator' => '<span class="divider">/</span>',
	'htmlOptions' => array('class' => 'breadcrumb')
));

/*$this->menu=array(
	array('label'=>'Create Emaillist','url'=>array('create')),
	array('label'=>'Manage Emaillist','url'=>array('admin')),
);
*/
?>

<h1>Emaillists</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
