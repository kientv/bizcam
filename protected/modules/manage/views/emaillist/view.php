<?php
$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
	'homeLink' => false,
	'links' => array(
		$model->name,
	),
	'separator' => '<span class="divider">/</span>',
	'htmlOptions' => array('class' => 'breadcrumb')
));
/*
$this->menu=array(
	array('label'=>'List Emaillist','url'=>array('index')),
	array('label'=>'Create Emaillist','url'=>array('create')),
	array('label'=>'Update Emaillist','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Emaillist','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Emaillist','url'=>array('admin')),
);
*/
?>

<h1>View Emaillist #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'name',
		'description',
		'created_time',
	),
)); ?>
