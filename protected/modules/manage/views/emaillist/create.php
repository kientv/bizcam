<?php
$this->breadcrumbs=array(
	'Emaillists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Emaillist','url'=>array('index')),
	array('label'=>'Manage Emaillist','url'=>array('admin')),
);
?>

<h1>Create Emaillist</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>