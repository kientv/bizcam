<?php
$this->breadcrumbs=array(
	'Emaillists'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Emaillist','url'=>array('index')),
	array('label'=>'Create Emaillist','url'=>array('create')),
	array('label'=>'View Emaillist','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Emaillist','url'=>array('admin')),
);
?>

<h1>Update Emaillist <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>