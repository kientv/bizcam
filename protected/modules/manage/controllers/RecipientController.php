<?php
class RecipientController extends RController
{
	public $layout = 'manage.views.layouts.main';
	public function actionIndex()
	{
		$criteriaGrid = new CDbCriteria();
		$criteriaGrid->alias = 'tbl_recipient';
		$criteriaGrid->select = '*';
		$criteriaGrid->compare('campaign_id', $_REQUEST['id']);
		$criteriaGrid->compare('confirmed', '1');
		$criteriaGrid->addCondition('to_address NOT IN (SELECT email FROM tbl_unsubscribed)');
		$dataProvider = new CActiveDataProvider('Recipient', array('criteria' => $criteriaGrid, 'pagination' => array('pageSize' => 25)));
		
		$recipient = new Recipient();
		$recipient->campaign_id = $_REQUEST['id'];
		$campaignName = $recipient->campaign->name;
		$this->render('index', array('campaignName' => $campaignName, 'dataProvider' => $dataProvider));		
	}
	
	public function actionImport()
	{
		$this->renderPartial('import');		
	}
	
	/**
	 * Checkes whether the request is a CGridView ajax one. If so, render only the view withouth processing its scripts
	 * again.
	 * @param $model the campaign ActiveRecord
	 */
	protected function checkGridViewUpdate($model)
	{
		$ajax = Yii::app()->getRequest()->getParam('ajax');
		if ($ajax == 'campaign-users-grid')
		{
			$recipient = new Recipient();
			if (Yii::app()->getRequest()->getParam('campaign_id'))
			{
				$user->setAttributes(Yii::app()->getRequest()->getParam('campaign_id'), false);
			}
			$criteria = new CDbCriteria;
			$criteria->alias = '{{recipient}}';
			$criteria->compare('to_name', $campaignEmail->to_name, true);
			$criteria->compare('to_address', $campaignEmail->to_address, true);
			$criteria->compare('confirmed', '1');
			$criteria->addCondition('to_address NOT IN (SELECT email FROM tbl_unsubscribed)');
			$dataProvider = new CActiveDataProvider('Recipient', array('criteria' => $criteria, 'pagination' => array('pageSize' => 25)));

			$this->renderPartial('Recipient', array('Recipient' => $recipient, 'dataProvider' => $dataProvider));

			Yii::app()->end();
		}
	}
}