<?php

class DefaultController extends CController
{
	public $layout = 'main';

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public function getPageTitle()
	{
		if($this->action->id==='index')
			return Yii::app()->name;
		else
			return Yii::app()->name . ' - '.ucfirst($this->action->id);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionExport()
	{
		$this->render('export', array('package'=>new Package(), 'author'=>new Author()));
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout(false);
		$this->redirect(Yii::app()->createUrl('/index.php'));
	}
}