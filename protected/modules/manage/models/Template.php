<?php

/**
 * This is the model class for table "{{template}}".
 *
 * The followings are the available columns in table '{{template}}':
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $description
 * @property string $image_src
 * @property string $html_body
 * @property string $text_body
 * @property string $variables
 */
class Template extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Template the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{template}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, html_body', 'required'),
			array('name, variables', 'length', 'max'=>255),
			array('description, image_src', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, image_src, html_body, text_body, variables', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'image_src' => 'Image Src',
			'html_body' => 'Html Body',
			'text_body' => 'Text Body',
			'variables' => 'Variables',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($user_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$user_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image_src',$this->image_src,true);
		$criteria->compare('html_body',$this->html_body,true);
		$criteria->compare('text_body',$this->text_body,true);
		$criteria->compare('variables',$this->variables,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns template name
	 * @static
	 * @param int $template
	 * @return bool
	 */
	public static function getTemplateName($template = self::BLANK)
	{
		return self::_getValue($template, 'text');
	}

	/**
	 * Returns the preview
	 * @static
	 * @param int $template
	 * @return bool|string
	 */
	public static function getView($template)
	{
		echo Yii::app()->db
			->createCommand("SELECT html_body FROM tbl_template WHERE id=:template_id")
			->bindValue(":template_id", $template)
			->queryScalar();
	}

	/**
	 * Returns javascript data options for ddslick
	 * @static
	 * @param $assetsUrl
	 * @param int $selected
	 * @return string
	 */
	public static function getJSData($assetsUrl, $template_id = 1)
	{
		$data = array();
		
		$templates = array();
		
		$result = Yii::app()->db->createCommand('SELECT * FROM tbl_template WHERE user_id=0 OR user_id = ' . Yii::app()->User->id)->queryAll();
		foreach ($result as $key => $value) {
			$templates[] = array('text' => $value['name'],
				'description' => $value['description'],
				'imageSrc' => $assetsUrl . '/images/thumbnair/' . $value['image_src'],
				'mainView' => $value['html_body'],
				'bodyView' => $value['text_body'],
				'id' => $value['id']
			);
		};
		
		foreach ($templates as $key => $value) {
			$data[] = CMap::mergeArray(
				array_slice($value, 0, 5, true),
				array('imageSrc' => $value['imageSrc'], 'value' => $value['id'], 'selected' => ($selected == $template_id))
			);
		}
		
		return CJSON::encode($data);
	}

	/**
	 * Returns a specific value in the $templates array specifying the template key
	 * @static
	 * @param $key
	 * @param $value
	 * @return bool
	 */
	protected static function _getValue($key, $value)
	{
		//return array_key_exists($key, self::$templates) ? self::$templates[$key][$value] : false;
	}
}