<?php

/**
 * This is the model class for table "{{campaign_email}}".
 *
 * The followings are the available columns in table '{{campaign_email}}':
 * @property integer $id
 * @property integer $campaign_id
 * @property integer $status
 * @property string $to_address
 * @property string $to_name
 * @property integer $create_time
 * @property integer $send_time
 * @property integer $opened
 * @property integer $confirmed
 */
class CampaignEmail extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CampaignEmail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{campaign_email}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('campaign_id, status, to_address, to_name, create_time, send_time, opened', 'required'),
			array('campaign_id, status, create_time, send_time, opened, confirmed', 'numerical', 'integerOnly'=>true),
			array('to_address', 'length', 'max'=>200),
			array('to_name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, campaign_id, status, to_address, to_name, create_time, send_time, opened, confirmed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'campaign_id' => 'Campaign',
			'status' => 'Status',
			'to_address' => 'To Address',
			'to_name' => 'To Name',
			'create_time' => 'Create Time',
			'send_time' => 'Send Time',
			'opened' => 'Opened',
			'confirmed' => 'Confirmed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('campaign_id',$this->campaign_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('to_address',$this->to_address,true);
		$criteria->compare('to_name',$this->to_name,true);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('send_time',$this->send_time);
		$criteria->compare('opened',$this->opened);
		$criteria->compare('confirmed',$this->confirmed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}