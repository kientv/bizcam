-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 30, 2013 at 01:11 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bizcam`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_authassignment`
--

CREATE TABLE IF NOT EXISTS `tbl_authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_authassignment`
--

INSERT INTO `tbl_authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '1', NULL, NULL),
('customer', '2', NULL, 'N;'),
('customer', '1', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_authitem`
--

CREATE TABLE IF NOT EXISTS `tbl_authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_authitem`
--

INSERT INTO `tbl_authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'administrator', NULL, 'N;'),
('customer', 2, 'Customer', NULL, 'N;'),
('Site.*', 1, NULL, NULL, 'N;'),
('Manage.Amazon.*', 1, NULL, NULL, 'N;'),
('Manage.Campaign.*', 1, NULL, NULL, 'N;'),
('Manage.Default.*', 1, NULL, NULL, 'N;'),
('Manage.Message.*', 1, NULL, NULL, 'N;'),
('Manage.Unsubscribed.*', 1, NULL, NULL, 'N;'),
('Store.Categories.*', 1, NULL, NULL, 'N;'),
('Store.Store.*', 1, NULL, NULL, 'N;'),
('User.Activation.*', 1, NULL, NULL, 'N;'),
('User.Admin.*', 1, NULL, NULL, 'N;'),
('User.Default.*', 1, NULL, NULL, 'N;'),
('User.Login.*', 1, NULL, NULL, 'N;'),
('User.Logout.*', 1, NULL, NULL, 'N;'),
('User.Profile.*', 1, NULL, NULL, 'N;'),
('User.ProfileField.*', 1, NULL, NULL, 'N;'),
('User.Recovery.*', 1, NULL, NULL, 'N;'),
('User.Registration.*', 1, NULL, NULL, 'N;'),
('User.User.*', 1, NULL, NULL, 'N;'),
('Site.Index', 0, NULL, NULL, 'N;'),
('Site.Error', 0, NULL, NULL, 'N;'),
('Site.Contact', 0, NULL, NULL, 'N;'),
('Site.Login', 0, NULL, NULL, 'N;'),
('Site.Logout', 0, NULL, NULL, 'N;'),
('Manage.Amazon.Manage', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Manage', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Create', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Update', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Stats', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Delete', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Users', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Preview', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Template', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Test', 0, NULL, NULL, 'N;'),
('Manage.Campaign.TestCommand', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Upload', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Gallery', 0, NULL, NULL, 'N;'),
('Manage.Campaign.Thumb', 0, NULL, NULL, 'N;'),
('Manage.Default.Index', 0, NULL, NULL, 'N;'),
('Manage.Default.Export', 0, NULL, NULL, 'N;'),
('Manage.Default.Error', 0, NULL, NULL, 'N;'),
('Manage.Default.Logout', 0, NULL, NULL, 'N;'),
('Manage.Message.Image', 0, NULL, NULL, 'N;'),
('Manage.Unsubscribed.Manage', 0, NULL, NULL, 'N;'),
('Manage.Unsubscribed.Create', 0, NULL, NULL, 'N;'),
('Manage.Unsubscribed.Delete', 0, NULL, NULL, 'N;'),
('Manage.Unsubscribed.Form', 0, NULL, NULL, 'N;'),
('Store.Categories.View', 0, NULL, NULL, 'N;'),
('Store.Categories.Create', 0, NULL, NULL, 'N;'),
('Store.Categories.Update', 0, NULL, NULL, 'N;'),
('Store.Categories.Delete', 0, NULL, NULL, 'N;'),
('Store.Categories.Index', 0, NULL, NULL, 'N;'),
('Store.Categories.Admin', 0, NULL, NULL, 'N;'),
('Store.Store.View', 0, NULL, NULL, 'N;'),
('Store.Store.Create', 0, NULL, NULL, 'N;'),
('Store.Store.Update', 0, NULL, NULL, 'N;'),
('Store.Store.Delete', 0, NULL, NULL, 'N;'),
('Store.Store.Index', 0, NULL, NULL, 'N;'),
('Store.Store.Admin', 0, NULL, NULL, 'N;'),
('User.Activation.Activation', 0, NULL, NULL, 'N;'),
('User.Admin.Admin', 0, NULL, NULL, 'N;'),
('User.Admin.View', 0, NULL, NULL, 'N;'),
('User.Admin.Create', 0, NULL, NULL, 'N;'),
('User.Admin.Update', 0, NULL, NULL, 'N;'),
('User.Admin.Delete', 0, NULL, NULL, 'N;'),
('User.Default.Index', 0, NULL, NULL, 'N;'),
('User.Login.Login', 0, NULL, NULL, 'N;'),
('User.Logout.Logout', 0, NULL, NULL, 'N;'),
('User.Profile.Profile', 0, NULL, NULL, 'N;'),
('User.Profile.Edit', 0, NULL, NULL, 'N;'),
('User.Profile.Changepassword', 0, NULL, NULL, 'N;'),
('User.ProfileField.View', 0, NULL, NULL, 'N;'),
('User.ProfileField.Create', 0, NULL, NULL, 'N;'),
('User.ProfileField.Update', 0, NULL, NULL, 'N;'),
('User.ProfileField.Delete', 0, NULL, NULL, 'N;'),
('User.ProfileField.Admin', 0, NULL, NULL, 'N;'),
('User.Recovery.Recovery', 0, NULL, NULL, 'N;'),
('User.Registration.Registration', 0, NULL, NULL, 'N;'),
('User.User.View', 0, NULL, NULL, 'N;'),
('User.User.Index', 0, NULL, NULL, 'N;'),
('Guest', 2, 'Guest', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_authitemchild`
--

CREATE TABLE IF NOT EXISTS `tbl_authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_authitemchild`
--

INSERT INTO `tbl_authitemchild` (`parent`, `child`) VALUES
('customer', 'Manage.Amazon.Manage'),
('customer', 'Manage.Campaign.Create'),
('customer', 'Manage.Campaign.Delete'),
('customer', 'Manage.Campaign.Gallery'),
('customer', 'Manage.Campaign.Manage'),
('customer', 'Manage.Campaign.Preview'),
('customer', 'Manage.Campaign.Stats'),
('customer', 'Manage.Campaign.Template'),
('customer', 'Manage.Campaign.Test'),
('customer', 'Manage.Campaign.TestCommand'),
('customer', 'Manage.Campaign.Thumb'),
('customer', 'Manage.Campaign.Update'),
('customer', 'Manage.Campaign.Upload'),
('customer', 'Manage.Campaign.Users'),
('customer', 'Manage.Default.Error'),
('customer', 'Manage.Default.Export'),
('customer', 'Manage.Default.Index'),
('customer', 'Manage.Default.Logout'),
('customer', 'Manage.Message.Image'),
('customer', 'Manage.Unsubscribed.Create'),
('customer', 'Manage.Unsubscribed.Delete'),
('customer', 'Manage.Unsubscribed.Form'),
('customer', 'Manage.Unsubscribed.Manage'),
('customer', 'Site.Error'),
('customer', 'Site.Index'),
('customer', 'Site.Login'),
('customer', 'Site.Logout'),
('customer', 'Store.Store.Admin'),
('customer', 'Store.Store.Index'),
('customer', 'Store.Store.View'),
('customer', 'User.Activation.Activation'),
('customer', 'User.Default.Index'),
('customer', 'User.Login.Login'),
('customer', 'User.Logout.*'),
('customer', 'User.Logout.Logout'),
('customer', 'User.Profile.Changepassword'),
('customer', 'User.Profile.Edit'),
('customer', 'User.Recovery.Recovery'),
('customer', 'User.User.Index'),
('customer', 'User.User.View'),
('Guest', 'Store.Store.Index'),
('Guest', 'Store.Store.View');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_campaign`
--

CREATE TABLE IF NOT EXISTS `tbl_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `utm_source` varchar(200) NOT NULL,
  `utm_medium` varchar(200) NOT NULL,
  `utm_term` varchar(200) NOT NULL,
  `utm_content` varchar(200) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `body_html` text NOT NULL,
  `body_text` text NOT NULL,
  `custom` text NOT NULL,
  `to_subscribers` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `total_sent` int(11) NOT NULL,
  `total_failed` int(11) NOT NULL,
  `total_list` int(11) NOT NULL,
  `total_opened` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `scheduled_for` int(11) NOT NULL,
  `sent_at` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_campaign`
--

INSERT INTO `tbl_campaign` (`id`, `name`, `utm_source`, `utm_medium`, `utm_term`, `utm_content`, `subject`, `body_html`, `body_text`, `custom`, `to_subscribers`, `template_id`, `total_sent`, `total_failed`, `total_list`, `total_opened`, `create_time`, `scheduled_for`, `sent_at`, `status`) VALUES
(2, 'Chiến dịch đầu xuân', 'campaign', 'email', '', '', 'Giảm 50% giá quần jean', 'Gửi thư\r\nTừ bizcam.vn', 'Body Text', 'kientranvan@gmail.com, vankien.tran@equest.edu.vn', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 1376409600, '', 1),
(3, 'Giảm 50% giá áo sơ mi', 'campaign', 'email', '', '', 'Giảm 50% giá áo sơ mi', 'Body HTML', 'Body Text', 'kientranvan@gmail.com, kientv@facebook.com', 1, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 1376496000, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_campaign_email`
--

CREATE TABLE IF NOT EXISTS `tbl_campaign_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `from_address` varchar(200) NOT NULL,
  `from_name` varchar(100) NOT NULL,
  `to_address` varchar(200) NOT NULL,
  `to_name` varchar(100) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `text_body` text NOT NULL,
  `html_body` text NOT NULL,
  `create_time` int(11) NOT NULL,
  `send_time` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `opened` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_campaign_email`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_campaign_unsubscribed`
--

CREATE TABLE IF NOT EXISTS `tbl_campaign_unsubscribed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_campaign_unsubscribed`
--

INSERT INTO `tbl_campaign_unsubscribed` (`id`, `email`, `create_time`) VALUES
(5, 'hau.tc.1092@gmail.com', 1377196080);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `note` varchar(200) DEFAULT NULL,
  `ord_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `name`, `note`, `ord_view`) VALUES
(1, 'Email marketing', NULL, 1),
(2, 'Digital marketing', NULL, 2),
(3, 'Social marketing', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email`
--

CREATE TABLE IF NOT EXISTS `tbl_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maillist_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `note` varchar(200) DEFAULT NULL,
  `sent_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_email`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_maillist`
--

CREATE TABLE IF NOT EXISTS `tbl_maillist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_maillist`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_package`
--

CREATE TABLE IF NOT EXISTS `tbl_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_package`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles`
--

CREATE TABLE IF NOT EXISTS `tbl_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_profiles`
--

INSERT INTO `tbl_profiles` (`user_id`, `lastname`, `firstname`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Demo', 'Demo');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles_fields`
--

CREATE TABLE IF NOT EXISTS `tbl_profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_profiles_fields`
--

INSERT INTO `tbl_profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rights`
--

CREATE TABLE IF NOT EXISTS `tbl_rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_rights`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_store`
--

CREATE TABLE IF NOT EXISTS `tbl_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(300) NOT NULL,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `tag` varchar(200) DEFAULT NULL,
  `viewer` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_store`
--

INSERT INTO `tbl_store` (`id`, `name`, `description`, `content`, `user_id`, `cat_id`, `tag`, `viewer`, `created`) VALUES
(1, '6 định dạng email marketing quan trọng – Email chào mời', 'Tên Tiếng Anh của email chào mời là Dedicated email, hoặc là stand-alone email. Như tôi đã nói ở những lần trước thì việc dịch 1 khái niệm từ Tiếng Anh sang Tiếng Việt đôi khi không được chính xác 100%, nhưng tôi sẽ trình bày chi tiết thì các bạn cũng nắm được ý nghĩa của nó', '<p>Email ch&agrave;o mời l&agrave; loại email m&agrave; c&aacute;c doanh nghiệp Việt Nam sử dụng  &ldquo;triệt để&rdquo; nhất khi chạy email marketing, v&agrave; n&oacute; cũng ch&iacute;nh l&agrave; loại email  bị người đưa v&agrave;o spam &ldquo;quyết liệt&rdquo; nhất. Đ&uacute;ng theo c&aacute;i nghĩa của n&oacute;,  email ch&agrave;o mời được d&ugrave;ng đề giới thiệu 1 sản phẩm, dịch vụ, khuyến m&atilde;i,  mời tham gia hội thảo,&hellip; n&agrave;o đ&oacute; đến người nhận, bao gồm một lời k&ecirc;u gọi  h&agrave;nh động (Call-to-action) b&ecirc;n trong (v&iacute; dụ như &ldquo;Xem ngay&rdquo;, &ldquo;Đăng k&iacute;  ngay&rdquo;,&hellip;)</p>\r\n<p>Loại email ch&agrave;o mời n&agrave;y thường được sử dụng để th&ocirc;ng b&aacute;o đến tất cả  mọi người những th&ocirc;ng tin quan trọng, hấp dẫn nhất đang hoặc sắp diễn  ra, n&ecirc;n thường được gửi đến to&agrave;n bộ cơ sở dữ liệu. C&aacute;c marketer cũng &iacute;t  quan t&acirc;m đến việc n&acirc;ng cao tỉ lệ chuyển đổi hoặc giảm tỉ lệ hủy đăng  nhận tin (unsubscribe) hơn so với những loại email kh&aacute;c.</p>\r\n<h2>Ưu điểm của email ch&agrave;o mời</h2>\r\n<h3>Tập trung v&agrave;o Lời k&ecirc;u gọi h&agrave;nh động (Call-to-action)</h3>\r\n<p>Kh&ocirc;ng giống như newsletter hoặc digest, email ch&agrave;o mời tập trung  thẳng v&agrave;o một lời k&ecirc;u gọi h&agrave;nh động n&agrave;o đ&oacute;. Người nhận email sẽ nhận  được những th&ocirc;ng tin đặc biệt, hấp dẫn, kh&ocirc;ng thể bỏ qua (&iacute;t nhất l&agrave;  theo người gửi) v&agrave; sẽ c&oacute; xu hướng l&agrave;m theo những g&igrave; người gửi y&ecirc;u cầu.</p>\r\n<h3>Đơn giản, dễ sử dụng</h3>\r\n<p>Do nội dung tập trung, đơn giản n&ecirc;n việc thiết kế ra email dạng n&agrave;y  cũng đơn giản hơn. Đ&ocirc;i khi đ&acirc;y cũng l&agrave; l&yacute; do m&agrave; c&aacute;c email marketer sử  dụng rất nhiều. Bạn chỉ cần t&oacute;m tắt c&aacute;c nội dung ở Landing Page, t&ocirc; m&agrave;u,  in đậm c&aacute;c ti&ecirc;u đề v&agrave; c&aacute;c th&ocirc;ng tin quan trọng, đ&iacute;nh k&egrave;m một hai h&igrave;nh  ảnh v&agrave; c&oacute; một n&uacute;t CTA (&ldquo;Đăng k&iacute; ngay&rdquo; hoặc &ldquo;Xem ngay&rdquo; hoặc &ldquo;Đăng k&iacute; để  giữ chỗ&rdquo;&hellip;). Với newsletter hay digest, bạn phải thiết kế, sắp xếp v&agrave; đặt  ưu ti&ecirc;n c&aacute;c mẩu tin kh&aacute;c nhau. C&ograve;n ở email ch&agrave;o mời, bạn chỉ c&oacute; 1 th&ocirc;ng  tin nhất qu&aacute;.</p>\r\n<h3>Dễ đo kết quả</h3>\r\n<p>Bạn chỉ c&oacute; 1 nội dung, một CTA, gửi 1 lần n&ecirc;n bạn sẽ thấy kết quả  ngay lập tức. C&aacute;c chỉ số CTR, lượt xem landing page, tỉ lệ chuyển đổi,  tỉ lệ ROI cũng rất dễ theo d&otilde;i.</p>\r\n<h2>Khuyết điểm của email ch&agrave;o mời</h2>\r\n<h3>Gửi đột ngột</h3>\r\n<p>Với newsletter, bạn c&oacute; thể sắp lịch gửi hằng tuần, v&iacute; dụ như mỗi s&aacute;ng  Thứ ba. Lợi &iacute;ch của việc n&agrave;y l&agrave; khiến người nhận c&oacute; th&oacute;i quen chờ đợi  những th&ocirc;ng tin c&oacute; &iacute;ch từ doanh nghiệp của bạn. C&ograve;n với email ch&agrave;o mời  n&agrave;y, bạn sẽ gửi t&ugrave;y theo c&aacute;c hoạt động, sự kiện đang diễn ra v&agrave; thường  sẽ kh&ocirc;ng c&oacute; lịch cụ thể. Khuyết điểm của vi&ecirc;c n&agrave;y người nhận sẽ kh&ocirc;ng c&oacute;  sự tr&ocirc;ng đợi v&agrave; chuẩn bị trước, họ sẽ trở n&ecirc;n ph&ograve;ng thủ hơn v&agrave; dễ đưa  bạn mục spam hơn.</p>\r\n<h3>&nbsp;Nội dung đơn nhất</h3>\r\n<p>Tuy nội dung đơn nhất c&oacute; ưu điểm v&agrave; tập trung v&agrave;o một Lời k&ecirc;u gọi  h&agrave;nh động, nhưng n&oacute; cũng c&oacute; khuyết điểm l&agrave; kh&ocirc;ng đưa một lời k&ecirc;u gọi  h&agrave;nh động kh&aacute;c v&agrave;o. V&iacute; dụ c&ocirc;ng ty của bạn c&oacute; nhiều hoạt động, sự kiện  quan trọng th&igrave; bạn phải tạo ra một email ch&agrave;o mời kh&aacute;c.</p>', 1, 1, 'email marketing', 47, '2013-08-25 15:49:43'),
(2, 'Email marketing cơ bản 3 – A/B testing', 'Ở 2 phần trước, chúng ta đã tìm hiểu về nguyên tắc cơ bản của email marketing và kịch bản email marketing. Ở phần 3 này, Thủ thuật Marketing sẽ giới thiệu một kĩ thuật cơ bản trong email marketing – A/B Testing.', '<p>Cũng n&oacute;i th&ecirc;m 1 ch&uacute;t, đa số c&aacute;c kiến thức về online marketing được  viết bằng Tiếng Anh. Do đ&oacute;, c&oacute; nhiều thuật ngữ rất kh&oacute; dịch sang Tiếng  Việt, n&ecirc;n Thủ thuật Makreing sẽ giữ nguy&ecirc;n Tiếng Anh để c&aacute;c bạn dễ d&agrave;ng  tra cứu tr&ecirc;n internet sau n&agrave;y. Nếu c&aacute;c bạn cảm thấy từ &agrave;o c&oacute; thể dịch  được th&igrave; để lại comment.</p>\r\n<h2>A/B Testing l&agrave; g&igrave;?</h2>\r\n<p>A/B Testing, c&ograve;n c&oacute; t&ecirc;n gọi kh&aacute;c l&agrave; split testing (kiểm tra ph&acirc;n  t&aacute;ch), l&agrave; một phương ph&aacute;p kiểm tra những biến (variable &ndash; tức l&agrave; 1 vấn  đề n&agrave;o đ&oacute; ảnh hưởng đến hiệu quả marketing) v&agrave; so s&aacute;ch ch&uacute;ng với nhau để  tối ưu hiệu quả. V&iacute; dụ, trong email marketing, c&aacute;ch viết ti&ecirc;u đề  (title) l&agrave; một biến ảnh hướng đến tỉ lệ mở email, do đ&oacute;, bạn sẽ l&agrave;m A/B  Testing với ti&ecirc;u đề để x&aacute;c định xem c&aacute;ch viết n&agrave;o đem lại tỉ lệ mở email  cao nhất.</p>\r\n<p>Trong A/B Testing c&oacute; 2 thuật ngữ l&agrave; &ldquo;Control&rdquo; v&agrave; &ldquo;Treatment&rdquo;</p>\r\n<ul>\r\n    <li><em>Control (A)</em>: l&agrave; những nội dung (landing page, email, call-to-action) hiện tại, chưa thay đổi v&agrave; cần thực hiện A/B Testing.</li>\r\n    <li><em>Treatment (B)</em>: l&agrave; những nội dung đ&atilde; được thay đổi v&agrave; bạn nghĩ n&oacute; mang lại kết quả tốt hơn Control.</li>\r\n</ul>\r\n<p><a rel="attachment wp-att-1163" href="http://thuthuatmarketing.com/digital-marketing/email/email-marketing-co-ban-3-ab-testing/attachment/email-marketing-a-b-testing-thu-thuat-marketing/"><img width="600" height="538" src="http://thuthuatmarketing.com/wp-content/uploads/2013/01/email-marketing-a-b-testing-th%E1%BB%A7-thu%E1%BA%ADt-marketing.jpg" alt="email marketing - a b testing" class="aligncenter size-full wp-image-1163" /></a></p>\r\n<p>Sử dụng A/B Testing khi bắt đầu chạy c&aacute;c hoạt động marketing l&agrave; c&aacute;ch  rất tốt đề bạn biết được c&aacute;ch tăng lượng truy cập cho website v&agrave; tạo ra  nhiều kh&aacute;ch h&agrave;ng tiềm năng từ lượng truy cập đ&oacute;. Trong online marketing,  c&oacute; 1 v&agrave;i nội dung quan trọng trong việc thu h&uacute;t số lượng truy cập  &nbsp;nhưng <a target="_blank" href="http://thuthuatmarketing.com/digital-marketing/website/landing-page-la-gi/" title="Landing page l&agrave; g&igrave;?">landing page</a>,  email hoặc call-to-action. Nếu sử dụng A/B Testing một c&aacute;ch nghi&ecirc;m t&uacute;c,  bạn c&oacute; thể cải thiện c&aacute;c tỉ lệ chuyển đổi rất nhiều. Thực tế cho thấy,  A/B Testing tr&ecirc;n landing page c&oacute; thể tạo ra th&ecirc;m 30-40% kh&aacute;ch h&agrave;ng tiềm  năng ở những website B2B v&agrave; 20-25% ở c&aacute;c website Thương mại điện tử.</p>\r\n<p>Hơn nữa, việc l&agrave;m tăng nguồn thu h&uacute;t kh&aacute;ch h&agrave;ng tiềm năng cũng ch&iacute;nh  l&agrave; l&agrave;m tăng lợi thế cạnh tranh cho doanh nghiệp. Trong thực thế, cơ hội  n&agrave;y ch&iacute;nh l&agrave; khi c&aacute;c đối thủ kh&ocirc;ng sử dụng A/B Testing đ&uacute;ng c&aacute;ch.</p>\r\n<blockquote>\r\n<p><strong>Chỉ c&oacute; 40% marketer đ&aacute;nh gi&aacute; lại c&aacute;c kết quả kiểm tra.</strong></p>\r\n<p><em>Marketingsherpa Landing Page Optimization Benchmark Survey, 2011</em></p>\r\n</blockquote>\r\n<h2>Ph&acirc;n biệt A/B Testing v&agrave; Multivariate Testing</h2>\r\n<p>Mặc d&ugrave; 2 thuật ngữ n&agrave;y thường xuy&ecirc;n xuất hiện c&ugrave;ng nhau trong  marketing, nhưng A/B Testing c&oacute; những điểm kh&aacute;c so với multivariate  testing (kiểm tra đa biến).</p>\r\n<p><strong>A/B Testing</strong> cho ph&eacute;p bạn theo d&otilde;i chỉ 1 biến ở c&ugrave;ng 1  thời điểm. N&oacute; thường d&ugrave;ng để chọn ra biến n&agrave;o mang lại kết quả tốt  nhất. V&iacute; dụ, nếu bạn kiểm tra m&agrave;u nền của landing page để x&aacute;c định m&agrave;u  n&agrave;o tạo ra được nhiều kh&aacute;ch h&agrave;ng tiềm năng hơn, bạn c&oacute; thể tiến h&agrave;nh  kiểm tra biến &ldquo;m&agrave;u sắc&rdquo; với 2 m&agrave;u &ldquo;xanh&rdquo; v&agrave; &ldquo;đỏ&rdquo;. Bạn cũng c&oacute; thể kiểm  tra việc ph&acirc;n phối m&agrave;u ở to&agrave;n trang, khi đ&oacute; việc ph&acirc;n phối m&agrave;u của cả  trang ch&iacute;nh l&agrave; 1 biến, chứ kh&ocirc;ng hằn 1 m&agrave;u l&agrave; 1 biến.</p>\r\n<p>Ngược lại, <strong>Multivariate testing</strong> cho ph&eacute;p bạn kiểm  tra nhiều biến đồng thời. Tr&ecirc;n l&yacute; thuyết th&igrave; multivariate testing l&agrave; sự  kết hợp của một chuỗi c&aacute;c A/B Testing diễn ra đồng thời. Tuy nhi&ecirc;n, để  thực hiện một multivariate testing hiệu quả v&agrave; c&oacute; &yacute; nghĩa thống k&ecirc;, bạn  cần c&oacute; một lượng truy cập lớn (như kiểu của Google v&agrave; Youtube).</p>\r\n<div style="width: 610px" class="wp-caption aligncenter" id="attachment_1160"><a rel="attachment wp-att-1160" href="http://thuthuatmarketing.com/digital-marketing/email/email-marketing-co-ban-3-ab-testing/attachment/email-marketing-a-b-testing-paragon-thu-thuat-marketing/"><img width="600" height="470" src="http://thuthuatmarketing.com/wp-content/uploads/2013/01/email-marketing-a-b-testing-Paragon-th%E1%BB%A7-thu%E1%BA%ADt-marketing.jpg" alt=" email marketing - a b testing - Paragon" class="size-full wp-image-1160" /></a>\r\n<p class="wp-caption-text">Một mẫu A/B Testing với Control &amp; Treatment</p>\r\n</div>\r\n<h2>10 điều quan trọng để c&oacute; một A/B Testing hiệu quả</h2>\r\n<p>#1 &ndash; 1 lần chỉ thực hiện 1 kiểm tra</p>\r\n<p>#2 &ndash; 1 lần chi kểm tra 1 biến</p>\r\n<p>#3 &ndash; Kiểm tra cả những nội dung chi tiết</p>\r\n<p>#4 &ndash; Bạn c&oacute; thể kiểm tra một nh&oacute;m thay đổi (v&iacute; dụ như c&aacute;c mẫu thiết kế landing page kh&aacute;c nhau)</p>\r\n<p>#5 &ndash; Đo lường sự ảnh hưởng v&agrave;o phễu chuyển đổi c&agrave;ng s&acirc;u c&agrave;ng tốt</p>\r\n<p>#6 &ndash; Thiết lập control v&agrave; treatment.</p>\r\n<p>#7 &ndash; Quyết định m&igrave;nh cần kiểm tra những g&igrave;</p>\r\n<p>#8 &ndash; Ph&acirc;n t&aacute;ch mẫu kiểm tra ngẫu nhi&ecirc;n</p>\r\n<p>#9 &ndash; Thực hiện kiểm tra v&agrave;o c&ugrave;ng thời gian</p>\r\n<p>#10 &ndash; X&aacute;c định những nội dung quan trọng v&agrave; cần thiết trước khi kiểm tra</p>', 1, 1, '', 16, '2013-08-25 15:50:55'),
(3, ' Email marketing là gì?', 'Email marketing là hình thức gửi email thông tin/quảng cáo có nội dung thông tin liên quan tới người nhận đã đồng ý đăng kí nhận email (opt-in) trực tiếp hay gián tiếp và cho phép họ có quyền không tiếp tục nhận email quảng cáo nữa.', '<p>Email marketing l&agrave; một c&ocirc;ng cụ marketing  rất hiệu quả v&agrave; sẽ mang lại ROI (Return on Investment) như mong đợi nếu  &aacute;p dụng b&agrave;i bản v&agrave; đ&uacute;ng c&aacute;ch v&igrave; email marketing gi&uacute;p bạn:</p>\r\n<ul>\r\n    <li style="text-align: justify;">Tiết kiệm chi ph&iacute; &iacute;t nhất 75% chi ph&iacute; so với c&aacute;c h&igrave;nh thức quảng c&aacute;o kh&aacute;c</li>\r\n    <li style="text-align: justify;">Chi ph&iacute; d&agrave;nh cho quảng c&aacute;o từ lớn đến nhỏ đều c&oacute; thể sử dụng</li>\r\n    <li style="text-align: justify;">Gửi th&ocirc;ng điệp quảng c&aacute;o đến đ&uacute;ng kh&aacute;ch h&agrave;ng mục ti&ecirc;u</li>\r\n    <li style="text-align: justify;">C&oacute; thể c&aacute; nh&acirc;n h&oacute;a th&ocirc;ng điệp quảng c&aacute;o  để lấy được sự ch&uacute; &yacute; tuyệt đối của người nhận m&agrave; kh&ocirc;ng bị chi phối bởi  c&aacute;c th&ocirc;ng tin kh&aacute;c</li>\r\n    <li style="text-align: justify;">Email l&agrave; k&ecirc;nh giao tiếp được kh&aacute;ch h&agrave;ng  sử dụng nhiều nhất trong ng&agrave;y v&agrave; đ&atilde; trở th&agrave;nh một phần thiết yếu trong  cuộc sống v&agrave; c&ocirc;ng việc của nhiều người</li>\r\n    <li style="text-align: justify;">Chủ động điều chỉnh thời gian, thời điểm truyền tải th&ocirc;ng tin tới kh&aacute;ch h&agrave;ng mục ti&ecirc;u</li>\r\n    <li style="text-align: justify;">Cho ph&eacute;p lan truyền th&ocirc;ng điệp quảng c&aacute;o bằng h&igrave;nh thức forward</li>\r\n    <li style="text-align: justify;">Cho ph&eacute;p dễ d&agrave;ng lưu trữ th&ocirc;ng điệp quảng c&aacute;o để tham khảo sau n&agrave;y</li>\r\n    <li style="text-align: justify;">Ph&acirc;n phối th&ocirc;ng điệp nhanh ch&oacute;ng tức thời</li>\r\n    <li style="text-align: justify;">Khối lượng ph&acirc;n phối lớn</li>\r\n    <li style="text-align: justify;">Dễ d&agrave;ng thực hiện v&agrave; điều chỉnh c&aacute;c chiến lược, kế hoạch</li>\r\n    <li style="text-align: justify;">Cho ph&eacute;p nhận được phản hồi trực tiếp v&agrave; nhanh ch&oacute;ng</li>\r\n    <li style="text-align: justify;">Kh&ocirc;ng hạn chế h&igrave;nh thức thiết kế v&agrave; khối lượng nội dung của th&ocirc;ng điệp quảng c&aacute;o</li>\r\n    <li style="text-align: justify;">...</li>\r\n</ul>\r\n<p style="text-align: justify;">Với những đặc điểm đặc th&ugrave; như tr&ecirc;n th&igrave;  email marketing c&oacute; thể sử dụng cho c&aacute;c chương tr&igrave;nh c&oacute; li&ecirc;n quan tới  branding v&agrave; b&aacute;n h&agrave;ng như: giới thiệu sản phẩm / dịch vụ mới, c&aacute;c chương  tr&igrave;nh khuyến m&atilde;i, c&aacute;c hoạt động chăm s&oacute;c kh&aacute;ch h&agrave;ng v&agrave; c&aacute;c hoạt động  through the line (BTL + ATL). Hơn nữa, email marketing c&ograve;n ph&ugrave; hợp cho  c&aacute;c chương tr&igrave;nh c&oacute; t&iacute;nh chất th&ocirc;ng b&aacute;o v&agrave; c&aacute;c hoạt động PR &ndash; th&ocirc;ng c&aacute;o  b&aacute;o ch&iacute;.</p>', 1, 1, 'email marketing', 19, '2013-08-25 20:32:26'),
(4, 'Tại sao nên sử dụng Email Marketing?', 'Trong năm 2009, email marketing tiếp tục được xem là phương thức hữu ích và kinh tế nhất để tiếp cận các khách hàng và khách hàng tiềm năng.', '<h3>Triển vọng của email marketing</h3>\r\n<p>Một nghi&ecirc;n cứu MarketingSherpa cho thấy rằng 78% c&aacute;c nh&agrave; quảng c&aacute;o email marketing cho c&aacute;c doanh nghiệp v&agrave; 69% c&aacute;c nh&agrave; quảng c&aacute;o email marketing cho người ti&ecirc;u d&ugrave;ng vẫn nghĩ rằng email rất hiệu quả, v&agrave; t&aacute;c động của n&oacute; vẫn tiếp tục ph&aacute;t triển.</p>\r\n<p>&hellip; nhưng những  điều n&agrave;y c&oacute; nghĩa g&igrave; với bạn,những  chủ doanh nghiệp  nhỏ l&agrave;m việc chăm  chỉ? N&oacute; c&oacute; nghĩa l&agrave; đối thủ cạnh  tranh v&agrave; doanh  nghiệp m&agrave; bạn sử dụng  sản phẩm của họ h&agrave;ng ng&agrave;y (như ng&acirc;n  h&agrave;ng, nh&agrave;  cung cấp điện thoại di  động v&agrave; kế to&aacute;n) đều đang sử dụng  email  marketing để đạt được lợi thế  của họ.</p>\r\n<p>Bất kể bạn đang hoạt động trong lĩnh vực n&agrave;o, c&oacute; v&agrave;i c&aacute;ch bạn c&oacute;  thể   sử dụng email marketing để thu h&uacute;t những kh&aacute;ch h&agrave;ng mới v&agrave; biến họ   trở  th&agrave;nh những người truyền b&aacute; v&agrave; những người mua lặp lại l&acirc;u d&agrave;i.</p>\r\n<p>Dưới đ&acirc;y l&agrave; năm l&yacute; do tại sao bạn n&ecirc;n sử dụng email marketing để ph&aacute;t  triển c&aacute;c hoạt động quảng c&aacute;o kh&aacute;c của bạn:</p>\r\n<ul>\r\n    <li><strong>Cực k&igrave; hiệu quả.</strong> Email marketing hoạt động để niềm   tin  giữa bạn v&agrave; c&aacute;c kh&aacute;ch h&agrave;ng v&agrave; c&aacute;c kh&aacute;ch h&agrave;ng tiềm năng. Như ch&uacute;ng   t&ocirc;i đ&atilde;  thảo luận ở đ&acirc;y, một khi bạn đ&atilde; kiếm được niềm tin từ c&aacute;c kh&aacute;ch   h&agrave;ng  tiềm năng th&igrave; sẽ dễ d&agrave;ng hơn để b&aacute;n h&agrave;ng cho họ. Trong thực tế,   bạn thậm  ch&iacute; kh&ocirc;ng phải cung cấp cho họ &ldquo;những khuyến mại lớn&rdquo; bởi v&igrave;   trong t&acirc;m  tr&iacute; của họ, bạn giống như một người bạn tin cậy với c&aacute;c sản   phẩm v&agrave; dịch  vụ m&agrave; họ cần, chứ kh&ocirc;ng phải l&agrave; một tập đo&agrave;n kh&ocirc;ng thấy   mặt.</li>\r\n    <li><strong>C&oacute;  thể đo lường.</strong> L&agrave;m thế n&agrave;o để đo lường sự th&agrave;nh   c&ocirc;ng của một chiến  dịch quảng c&aacute;o truyền h&igrave;nh hay truyền thanh? Phỏng   đo&aacute;n sẽ l&agrave; hầu hết  c&acirc;u trả lời của mọi người. Với email marketing, bạn   c&oacute; thể thấy ch&iacute;nh  x&aacute;c bao nhi&ecirc;u người đ&atilde; mở email của bạn v&agrave; họ đ&atilde;  nhấp  v&agrave;o li&ecirc;n kết n&agrave;o  (nếu bạn c&oacute; một trang web). Bạn kh&ocirc;ng thể nhận  được  những th&ocirc;ng tin  chiến dịch c&oacute; thể đo lường n&agrave;y từ bất kỳ phương  tiện  truyền th&ocirc;ng n&agrave;o  kh&aacute;c.</li>\r\n    <li><strong>Tiết kiệm.</strong> Sử dụng BlinkContact, chỉ mất v&agrave;i xu để    gửi một email đến một kh&aacute;ch h&agrave;ng tiềm năng hoặc kh&aacute;ch h&agrave;ng hiện c&oacute; v&agrave;    bạn c&oacute; thể tự m&igrave;nh l&agrave;m. Bởi v&igrave; mọi người đ&atilde; tự nguyện đăng k&yacute; v&agrave;o danh    s&aacute;ch email của bạn, th&igrave; họ sẽ dễ tiếp nhận th&ocirc;ng điệp của bạn hơn bất  kỳ   c&aacute;ch thức n&agrave;o kh&aacute;c, như truyền h&igrave;nh, ph&aacute;t thanh v&agrave; b&aacute;o ch&iacute;.</li>\r\n    <li><strong>Dễ  d&agrave;ng để khởi đầu.</strong> BlinkContact c&oacute; mọi thứ bạn   cần để bắt đầu với  email marketing- thậm ch&iacute; nếu bạn kh&ocirc;ng c&oacute; một danh   s&aacute;ch địa chỉ email  n&agrave;o. Click v&agrave;o đ&acirc;y để thử BlinkContact miễn ph&iacute;   trong 30 ng&agrave;y, kh&ocirc;ng c&oacute;  điều kiện đi k&egrave;m.</li>\r\n    <li><strong>Vui sướng.</strong> Thực vậy! Thật kh&oacute; tả cảm  gi&aacute;c bạn nhận   được khi bạn gửi một email tới danh s&aacute;ch địa chỉ email của  m&igrave;nh v&agrave;  bạn  thấy &ndash; trong thời gian thực &ndash; c&oacute; bao nhi&ecirc;u người đang mở  email của  bạn  v&agrave; họ đang click v&agrave;o li&ecirc;n kết n&agrave;o. Email marketing thực sự l&agrave; một  trải  nghiệm rất dễ nghiện đấy!</li>\r\n</ul>\r\n<p>&nbsp;</p>', 1, 1, 'triển vọng', 14, '2013-08-25 20:35:13'),
(5, 'Đánh giá hiệu quả của chiến dịch tiếp thị bằng thư điện tử', 'Trong thời đại thương mại điện tử hiện nay, một trong những công việc mà các doanh nghiệp thường xuyên phải thực hiện là đánh giá hiệu quả các chiến dịch tiếp thị bằng email.', '<p>Theo chuy&ecirc;n gia về email  marketing của tạp ch&iacute; Entrepreneur ki&ecirc;m Tổng gi&aacute;m đốc điều h&agrave;nh c&ocirc;ng ty  cung cấp dịch vụ tiếp thị cho doanh nghiệp nhỏ Constant Contact, Gail  Goodman, c&aacute;c doanh nghiệp c&oacute; thể đặt ra những c&acirc;u hỏi dưới đ&acirc;y để đ&aacute;nh  gi&aacute; hiệu quả của c&aacute;c chiến dịch tiếp thị bằng thư điện tử.</p>\r\n<h1 style="text-align: justify;"><span style="font-size: medium;"><strong>1. Doanh nghiệp đ&atilde; đặt ra mục ti&ecirc;u g&igrave; cho c&aacute;c chiến dịch email marketing v&agrave; đ&atilde; đạt được c&aacute;c mục ti&ecirc;u n&agrave;y chưa?<br />\r\n</strong></span></h1>\r\n<h1 style="text-align: justify;"><span style="font-size: medium;">Doanh nghiệp đ&atilde; đ&aacute;nh gi&aacute; c&aacute;c kết quả đạt được từ c&aacute;c chiến dịch email marketing chưa? </span></h1>\r\n<p style="text-align: justify;"><span style="font-size: medium;">C&aacute;c  chiến dịch email marketing c&oacute; đem lại cho doanh nghiệp gi&aacute; trị n&agrave;o đ&aacute;ng  kể kh&ocirc;ng? Doanh nghiệp c&oacute; ph&aacute;t hiện ra những mục đ&iacute;ch hữu dụng kh&aacute;c của  hoạt động email marketing&nbsp;kh&ocirc;ng? Khi trả lời những c&acirc;u hỏi n&agrave;y, doanh  nghiệp đồng thời sẽ điều chỉnh được mục ti&ecirc;u cho hoạt động tiếp thị bằng  email cho chiến dịch tiếp theo.</span></p>\r\n<p style="text-align: justify;"><span style="font-size: medium;">[MA:  Mục ti&ecirc;u doanh nghiệp đặt ra chắc hẳn l&agrave; tăng trưởng doanh số sau khi  loạt thư điện tử được gửi đi. Tuy nhi&ecirc;n, chỉ cần nhận được thư phản hồi  của kh&aacute;ch h&agrave;ng cũng đ&aacute;ng mừng lắm rồi.]</span></p>\r\n<h1 style="text-align: justify;"><span style="font-size: medium;"><strong>2.  Doanh nghiệp c&oacute; ph&aacute;t triển được danh s&aacute;ch người nhận thư kh&ocirc;ng? v&agrave; danh  s&aacute;ch n&agrave;y c&oacute; cần được sắp xếp lại theo c&aacute;c ph&acirc;n kh&uacute;c thị trường mới  kh&ocirc;ng?</strong></span></h1>\r\n<p style="text-align: justify;"><span style="font-size: medium;">C&oacute; rất  nhiều cơ hội để ph&aacute;t triển danh s&aacute;ch những người nhận thư điện tử. H&atilde;y  tự hỏi doanh nghiệp đ&atilde; tận dụng được những cơ hội n&agrave;o trong số c&aacute;c cơ  hội sau đ&acirc;y:</span></p>\r\n<p style="text-align: justify;"><span style="font-size: medium;">* Tham gia c&aacute;c sự kiện, c&aacute;c buổi hội họp x&acirc;y dựng quan hệ;</span><br />\r\n<span style="font-size: medium;"> * Từ trang điện tử (website) của doanh nghiệp (c&oacute; thể th&ocirc;ng qua c&aacute;c mục đăng k&yacute; th&agrave;nh vi&ecirc;n hoặc nhận bản tin v.v&hellip;);</span><br />\r\n<span style="font-size: medium;"> * Th&ocirc;ng qua mối quan hệ của c&aacute;c nh&acirc;n vi&ecirc;n.</span></p>\r\n<p style="text-align: justify;"><span style="font-size: medium;">Doanh  nghiệp đ&atilde; ph&acirc;n loại c&aacute;c địa chỉ email của người nhận thư chưa? v&agrave; ph&acirc;n  loại ở mức độ n&agrave;o? Những c&acirc;u hỏi n&agrave;y sẽ gi&uacute;p doanh nghiệp ph&aacute;t triển  danh s&aacute;ch người nhận email trong chiến dịch sau v&agrave; ph&acirc;n loại danh s&aacute;ch  n&agrave;y theo từng ph&acirc;n kh&uacute;c thị trường th&iacute;ch hợp, nhằm n&acirc;ng cao hiệu quả của  c&aacute;c chiến dịch tiếp thị bằng email.</span></p>\r\n<p style="text-align: justify;"><span style="font-size: medium;">[MA:  Nếu gửi cho tất cả c&aacute;c kh&aacute;ch h&agrave;ng với thư điện tử c&oacute; c&ugrave;ng nội dung hiệu  quả chắc chắn sẽ thấp hơn nhiều khi với mỗi nh&oacute;m đối tượng doanh nghiệp  lại c&oacute; c&aacute;c điều chỉnh nội dung cho ph&ugrave; hợp với nhu cầu của họ. Soạn thảo  từng bức thư ri&ecirc;ng như c&aacute;ch của bạn Đạt c&oacute; lẽ sẽ đạt t&aacute;c động tốt nhất  tới người nhận, nhưng với danh s&aacute;ch kh&aacute;ch h&agrave;ng d&agrave;i dằng dặc th&igrave; t&iacute;nh  hiệu quả sao nhỉ?]</span></p>\r\n<h1 style="text-align: justify;"><span style="font-size: medium;"><strong>3. Email của doanh nghiệp c&oacute; mang t&iacute;nh nhất qu&aacute;n v&agrave; đem đến những gi&aacute; trị cao cho kh&aacute;ch h&agrave;ng kh&ocirc;ng?</strong></span><span style="font-size: medium;">&nbsp;</span></h1>\r\n<h1 style="text-align: justify;"><span style="font-size: medium;">Khi  tiếp thị bằng c&aacute;ch gửi email cho kh&aacute;ch h&agrave;ng, doanh nghiệp sẽ c&oacute; một cơ  hội rất lớn để giữ li&ecirc;n lạc thường xuy&ecirc;n với họ. C&aacute;c vấn đề m&agrave; doanh  nghiệp n&ecirc;n r&agrave; so&aacute;t lại l&agrave;: mức độ thường xuy&ecirc;n gửi email (thưa hay qu&aacute;  d&agrave;y), chất lượng của c&aacute;c chiến dịch tiếp thị bằng email, thời gian cần  thiết để đầu tư cho việc soạn thảo nội dung email. C&acirc;u hỏi ở đ&acirc;y l&agrave;: c&aacute;c  th&ocirc;ng tin cung cấp cho kh&aacute;ch h&agrave;ng c&oacute; gi&aacute; trị v&agrave; hữu dụng đối với họ  kh&ocirc;ng?</span></h1>\r\n<p style="text-align: justify;"><span style="font-size: medium;">[MA:  Vấn đề n&agrave;y nghe chừng kh&ocirc;ng đơn giản ch&uacute;t n&agrave;o. L&agrave;m sao để c&acirc;n bằng giữa  thường xuy&ecirc;n li&ecirc;n lạc v&agrave; kh&ocirc;ng tạo cảm gi&aacute;c bị l&agrave;m phiền ở kh&aacute;ch h&agrave;ng?  C&oacute; lẽ, đ&aacute;p ứng đầy đủ th&ocirc;ng tin trong thời gian ngắn nhất với c&aacute;c thư  phản hồi từ kh&aacute;ch h&agrave;ng l&agrave; c&aacute;ch tương đối hợp l&yacute;.]</span></p>\r\n<h1 style="text-align: justify;"><span style="font-size: medium;"><strong>4. Những chiến dịch email n&agrave;o đạt được kết quả tốt nhất v&agrave; xấu nhất? Nguy&ecirc;n nh&acirc;n?</strong></span></h1>\r\n<p style="text-align: justify;"><span style="font-size: medium;">Xem lại  những chủ đề n&agrave;o đ&atilde; được kh&aacute;ch h&agrave;ng quan t&acirc;m v&agrave; những chủ đề n&agrave;o kh&ocirc;ng  được họ ch&uacute; &yacute;? Doanh nghiệp đ&atilde; l&agrave;m điều g&igrave; để đ&aacute;p ứng lại sự quan t&acirc;m  của kh&aacute;ch h&agrave;ng? Thời điểm gửi thư tiếp thị c&oacute; t&aacute;c dụng nhất l&agrave; khi n&agrave;o?  Những th&ocirc;ng tin n&agrave;y sẽ gi&uacute;p doanh nghiệp định hướng tốt hơn cho c&aacute;c  chiến dịch tiếp thị bằng thư điện tử tiếp theo.</span></p>\r\n<p style="text-align: justify;"><span style="font-size: medium;">[MA: thường xuy&ecirc;n ghi ch&eacute;p, thống k&ecirc;, so s&aacute;nh kết quả l&agrave; th&oacute;i quen tốt.]</span></p>\r\n<h1 style="text-align: justify;"><span style="font-size: medium;"><strong>5. Doanh nghiệp đ&atilde; l&agrave;m g&igrave; để trở th&agrave;nh một chuy&ecirc;n gia hoạt động tiếp thị bằng email?</strong></span></h1>\r\n<p style="text-align: justify;"><span style="font-size: medium;">Doanh  nghiệp đ&atilde; hỏi thăm những người th&acirc;n, bạn b&egrave; về nhận x&eacute;t của họ đối với  c&aacute;c bức thư điện tử của m&igrave;nh chưa? Đ&atilde; t&igrave;m hiểu c&aacute;c phản hồi, đề xuất của  c&aacute;c kh&aacute;ch h&agrave;ng nhận được thư điện tử tiếp thị của doanh nghiệp chưa? Đ&atilde;  đọc c&aacute;c t&agrave;i liệu hay s&aacute;ch b&aacute;o n&oacute;i về tiếp thị bằng thư điện tử chưa?  Doanh nghiệp c&oacute; lắng nghe &yacute; kiến của c&aacute;c c&ocirc;ng ty kh&aacute;c về c&aacute;c chiến dịch  tiếp thị bằng thư điện tử kh&ocirc;ng? c&ocirc;ng việc n&oacute;i tr&ecirc;n c&oacute; tiến h&agrave;nh đồng  thời kh&ocirc;ng? V&igrave; vậy, tốt nhất chỉ n&ecirc;n đặt ra mục ti&ecirc;u thực hiện một đến  hai c&ocirc;ng việc trong số c&aacute;c c&ocirc;ng việc tr&ecirc;n để c&oacute; thể dần trở th&agrave;nh một  nh&agrave; tiếp thị bằng thư điện tử chuy&ecirc;n nghiệp.</span></p>\r\n<p style="text-align: justify;"><span style="font-size: medium;">[MA: Ngo&agrave;i việc tự ho&agrave;n thiện, doanh  nghiệp kh&ocirc;ng nhất thiết phải trở th&agrave;nh chuy&ecirc;n gia trong mọi việc. Nếu  tập trung v&agrave;o việc cung cấp sản phẩm tốt, dịch vụ chất lượng v&agrave; sử dụng  dịch vụ cố vấn tiếp thị từ một c&ocirc;ng ty chuy&ecirc;n nghiệp để c&oacute; kết quả cao  nhất th&igrave; cũng n&ecirc;n &aacute;p dụng.]</span></p>', 1, 1, 'đánh giá', 12, '2013-08-25 20:37:57'),
(6, 'Email marketing thành công', 'Nếu bạn đi bộ vào bất kì một trung tâm mua sắm trong thời gian cuối tháng 8, bạn nhanh chóng nhận ra rằng tất cả đều có những cửa hàng bán đồ dùng học tập cho học sinh: bút chì, sách vở, ba lô,…', '<p>đ&oacute; l&agrave; những c&ocirc;ng cụ cần thiết cho mỗi học sịnh. Vậy với chiến dịch email  marketing của bạn, bạn đ&atilde; suy nghĩ n&oacute; cần những g&igrave; để đạt được th&agrave;nh  c&ocirc;ng.</p>\r\n<p><strong><span style="font-size: large;">Form đăng k&yacute; tr&ecirc;n website</span></strong></p>\r\n<p>L&agrave; nơi bạn y&ecirc;u cầu kh&aacute;ch h&agrave;ng cung cấp th&ocirc;ng tin cần thiết như t&ecirc;n,  địa chỉ emails v&agrave; cam kết đồng &yacute; nhận email từ bạn. Đ&acirc;y l&agrave; phương thức  thu thập emails đem lại độ ch&iacute;nh x&aacute;c v&agrave; hiệu quả cao nhất</p>\r\n<h1>Email đầu ti&ecirc;n</h1>\r\n<p>Để tạo ấn tượng với kh&aacute;ch h&agrave;ng, sau mỗi form đăng k&yacute; được ho&agrave;n tất,  bạn n&ecirc;n c&oacute; email cảm ơn gửi tới kh&aacute;ch h&agrave;ng. Email n&agrave;y sẽ gi&uacute;p bạn tạo sự  th&acirc;n thiện v&agrave; cho thấy rằng bạn quan t&acirc;m tới kh&aacute;ch h&agrave;ng, v&agrave; bạn h&atilde;y nhớ  nhắc họ bạn sẽ gửi những g&igrave; v&agrave; mật độ như thế n&agrave;o cũng như lợi &iacute;ch khi  họ nhận emails của bạn. Ngắn gọn v&agrave; s&uacute;c t&iacute;ch l&agrave; điều kh&aacute;ch h&agrave;ng lu&ocirc;n  mong đợi</p>\r\n<h1>Ti&ecirc;u đề mạnh mẽ, cuốn h&uacute;t</h1>\r\n<p>Bạn chỉ c&oacute; một gi&acirc;y ngắn ngủi để thu h&uacute;t sự ch&uacute; &yacute; của một ai đ&oacute;, v&agrave;  ti&ecirc;u đề email cũng vậy. H&atilde;y s&aacute;ng tạo nhưng cũng cho họ biết họ đang nhận  được điều g&igrave; khi họ mở email.</p>\r\n<h1>Thương hiệu</h1>\r\n<p>Thương hiệu của bạn kh&ocirc;ng chỉ nằm trong t&ecirc;n người gửi, m&agrave; c&ograve;n trong  ti&ecirc;u đề email. H&atilde;y l&agrave;m r&otilde; thương hiệu của bạn để kh&aacute;ch h&agrave;ng nhận diện v&agrave;  khiến n&oacute; trở n&ecirc;n th&acirc;n thuộc với họ.</p>\r\n<h1>Nội dung email c&oacute; chủ đ&iacute;ch</h1>\r\n<p>Bạn phải c&oacute; những nội dung email li&ecirc;n quan tới người nhận. Bạn kh&ocirc;ng  được ph&eacute;p gửi những email c&oacute; nội dung kh&ocirc;ng đ&aacute;p ứng nhu cầu hay mong  muốn của họ.&nbsp; Bạn h&atilde;y nhớ rằng, email marketing l&agrave; c&ocirc;ng cụ gi&uacute;p bạn  hướng kh&aacute;ch h&agrave;ng tới website v&agrave; dịch vụ của bạn.</p>\r\n<h1>Call to Action</h1>\r\n<p>C&aacute;c CTAs lu&ocirc;n đ&oacute;ng vai tr&ograve; quan trọng v&agrave; định hướng kh&aacute;ch h&agrave;ng. Việc  thiết kế, sắp xếp c&aacute;c CTA để đưa kh&aacute;ch h&agrave;ng h&agrave;nh động theo đ&uacute;ng &yacute; muốn  của bạn sẽ tuyệt vời hơn rất nhiều với một email chỉ c&oacute; nội dung m&agrave;  kh&ocirc;ng c&oacute; lời k&ecirc;u gọi kh&aacute;ch h&agrave;ng h&agrave;nh động</p>\r\n<h1>T&iacute;ch hợp Social Media</h1>\r\n<p>T&iacute;ch hợp email marketing với Social Media, kh&aacute;ch h&agrave;ng c&oacute; thể chia sẻ  ch&iacute;nh nội dung email đ&oacute; l&ecirc;n trang c&aacute; nh&acirc;n của họ trong c&aacute;c mạng x&atilde; hội.  Social Network hiện tại c&oacute; tầm ảnh hưởng rất lớn tới xu hướng của người  sử dụng internet.</p>\r\n<h1>Thiết bị di động</h1>\r\n<p>Mẫu email của bạn được thiết kế rất đẹp mắt, nhưng n&oacute; c&oacute; tương th&iacute;ch  ho&agrave;n to&agrave;n với c&aacute;c thiết bị di động hay kh&ocirc;ng??? Rất may l&agrave; thị trường  smartphone đang c&oacute; sự tăng trưởng đ&aacute;ng kể n&ecirc;n việc check email v&agrave; hiển  thị email trở n&ecirc;n dễ d&agrave;ng hơn. Nhưng kh&ocirc;ng v&igrave; thế bạn kh&ocirc;ng quan t&acirc;m tới  mẫu thiết kế của m&igrave;nh -&nbsp; đơn giản &ndash; hiệu quả &ndash; ph&ugrave; hợp với mọi thiết bị  l&agrave; ưu điểm để bạn tiếp cận nhiều kh&aacute;ch h&agrave;ng hơn đối thủ cạnh tranh.</p>\r\n<h1>T&iacute;ch hợp Google Analytics</h1>\r\n<p>Bạn đ&atilde; c&oacute; t&agrave;i khoản GA để xem lưu lượng truy cập v&agrave;o website, v&agrave; bạn  muốn mỗi chiến dịch email của bạn sẽ t&aacute;c động trực tiếp l&ecirc;n kết quả của  google analytics. H&atilde;y quan t&acirc;m đến điều n&agrave;y, đ&acirc;y l&agrave; ti&ecirc;u ch&iacute; đ&aacute;nh gi&aacute;  hiệu quả nhất cho mỗi chiến dịch email.</p>\r\n<p>(Nguồn: Tạp ch&iacute; Email Marketing &ndash; Emailmarketing.com.vn)</p>', 1, 1, 'thành công', 12, '2013-08-25 20:40:53'),
(7, 'Bài học về Email Marketing hỗ trợ cho Mạng xã hội… từ Britney Spears?', 'Britney Spears dùng Email Marketing hỗ trợ cho mạng xã hội vì muốn có thêm nhiều followers trong công cuộc trở thành người được followed nhiều nhất trên Twitter.', '<p>Thay v&igrave; bỏ tiền mua c&aacute;c bảng quảng c&aacute;o hay l&agrave;m một chiến dịch&nbsp;<acronym title="Google Page Ranking">PR</acronym>&nbsp;oanh tạc dữ dội như Asthon Kucher, th&igrave; c&ocirc; đ&atilde; sử dụng k&ecirc;nh hiệu quả nhất: danh s&aacute;ch email marketing của m&igrave;nh.</p>\r\n<h1><strong><span style="font-size: large;">Tại sao đ&acirc;y l&agrave; một v&iacute; dụ tuyệt vời cho email marketing hỗ trợ mạng x&atilde; hội. H&atilde;y c&ugrave;ng ph&acirc;n t&iacute;ch một số điểm sau:</span><br />\r\n</strong></h1>\r\n<p>* D&ograve;ng ti&ecirc;u đề của email marketing c&oacute; th&ocirc;ng điệp v&agrave; mục đ&iacute;ch rất r&otilde; r&agrave;ng: &ldquo;Follow Britney on Twitter!&rdquo;</p>\r\n<p>* C&acirc;n đối giữa phần h&igrave;nh ảnh nhận diện v&agrave; phần text HTML.</p>\r\n<p>* Cung cấp gi&aacute; trị r&otilde; r&agrave;ng về l&yacute; do tại sao bạn n&ecirc;n follow Brit:  &ldquo;&hellip;get all the latest updates from Britney and her team&rdquo; (cập nhật những  tin tức mới nhất từ Britney v&agrave; đội của c&ocirc;)</p>\r\n<p>* Cung cấp 3 c&aacute;ch thức k&ecirc;u gọi-đến-h&agrave;nh động (h&igrave;nh ảnh, ngữ cảnh, v&agrave;  c&aacute;c link) tất cả đều dẫn đến trang Twitter của Brit v&agrave; cho link dẫn đến  trang đ&oacute; ngay lập tức.</p>\r\n<p>* Ngắn gọn v&agrave; ho&agrave;n to&agrave;n cụ thể &ndash; kh&ocirc;ng truyền tải th&ocirc;ng điệp thứ cấp để đ&aacute;nh lạc hướng người sử dụng.</p>\r\n<p>* T&ecirc;n email, d&ograve;ng ti&ecirc;u đề, v&agrave; c&aacute;c phần k&ecirc;u gọi-đến-h&agrave;nh động, click qua rất nhanh.</p>\r\n<p>* Thậm ch&iacute; c&ograve;n giải th&iacute;ch tại sao bạn nhận được email đ&oacute;.</p>\r\n<p>Đ&ocirc;i khi email marketing như một m&ocirc;n khoa học ch&iacute;nh x&aacute;c. Đ&ocirc;i khi lại kh&ocirc;ng ho&agrave;n to&agrave;n như vậy.</p>\r\n<h1><span style="font-size: large;"><strong>Vậy bạn c&oacute; đủ tự tin tr&ecirc;n c&aacute;c mạng x&atilde; hội để gửi một chiến dịch email marketing như vậy kh&ocirc;ng?</strong></span></h1>\r\n<p>Liệu danh s&aacute;ch email n&agrave;y đ&atilde; được gửi đi theo ph&acirc;n đoạn để loại bỏ  những người đ&atilde; follow Britney tr&ecirc;n Twitter hay chưa? C&oacute; lẽ l&agrave; chưa, d&ugrave;  t&ocirc;i ngờ rằng những người nhận đ&oacute; c&oacute; thể kh&oacute; chịu đấy.</p>\r\n<p>Hay liệu sự s&aacute;ng tạo d&ugrave;ng email marketing n&agrave;y c&oacute; mang t&iacute;nh c&aacute;ch mạng  hay kh&ocirc;ng? Kh&ocirc;ng, nhưng n&oacute; c&oacute; hiệu quả v&agrave; ph&ugrave; hợp trong suốt khu&ocirc;n khổ  mẫu email.</p>\r\n<p>Liệu bạn c&oacute; định quanh co n&oacute;i những điều xưa như tr&aacute;i đất, đại loại  như &ldquo;H&atilde;y tham gia c&ugrave;ng t&ocirc;i!&rdquo;? Vậy th&igrave; bạn n&ecirc;n xem lại ti&ecirc;u đề email của  m&igrave;nh! Hay bạn c&oacute; thực sự tạo được những gi&aacute; trị như c&aacute;ch email marketing  của Brit đ&atilde; l&agrave;m được hay kh&ocirc;ng? Nếu bạn kh&ocirc;ng thể tạo ra được gi&aacute; trị  th&igrave; phải xem x&eacute;t liệu bạn c&oacute; n&ecirc;n x&uacute;c tiến n&oacute; hay kh&ocirc;ng.</p>\r\n<p>Thay v&igrave; ch&egrave;n link Twitter v&agrave; Facebook v&agrave;o mọi thứ tr&ecirc;n mạng internet  n&agrave;y th&igrave; tại sao bạn lại kh&ocirc;ng l&agrave;m theo c&aacute;ch của Brit nhỉ, v&agrave; sử dụng một  mẫu email đơn giản để gửi tới những người nhận &ndash; những người y&ecirc;u qu&yacute;  bạn.</p>\r\n<p>Email marketing ho&agrave;n to&agrave;n c&oacute; thể gi&uacute;p bạn c&oacute; th&ecirc;m nhiều fan/follower hơn bất k&igrave; c&aacute;c phương thức n&agrave;o kh&aacute;c kết hợp lại.</p>', 1, 1, '', 14, '2013-08-25 20:43:07'),
(8, 'Câu chuyện thành công email marketing – Nikko Hà Nội', 'Nikko Hà Nội luôn đặt mục tiêu có được lượng khách hàng trung thành tăng trưởng hàng năm. Chính vì vậy, Nikko không ngừng tìm kiếm các phương thức nhằm nâng cao chất lượng phục vụ và chăm sóc khách hàng.', '<h1 style="text-align: justify;"><span style="font-size: large;"><strong>Giới thiệu C&ocirc;ng ty</strong></span></h1>\r\n<p style="text-align: justify;">Nikko H&agrave; Nội l&agrave; một th&agrave;nh vi&ecirc;n của hệ  thống kh&aacute;ch sạn Nikko quốc tế, điều h&agrave;nh bởi c&ocirc;ng ty kh&aacute;ch sạn JAL. C&aacute;ch  s&acirc;n bay quốc tế Nội B&agrave;i chừng 40 ph&uacute;t xe, Kh&aacute;ch sạn Nikko H&agrave; Nội nằm ở  trung t&acirc;m th&agrave;nh phố v&agrave; được bao quanh bởi nhiều hồ v&agrave; c&ocirc;ng vi&ecirc;n. Kh&aacute;ch  sạn c&oacute; c&aacute;c nh&agrave; h&agrave;ng, qu&aacute;n bar v&agrave; một trung t&acirc;m thể chất, ph&ograve;ng họp với  c&aacute;c trang thiết bị phục vụ hội nghị, một trung t&acirc;m thương vụ được trang  bị đầy đủ tiện nghi v&agrave; một bể bơi ngo&agrave;i trời. Kh&aacute;ch sạn c&oacute; tầm nh&igrave;n bao  qu&aacute;t hồ v&agrave; c&oacute; vị tr&iacute; thuận lợi nằm gần những t&ograve;a nh&agrave; văn c&oacute; tiếng, c&aacute;c  di t&iacute;ch lịch sử v&agrave; c&aacute;c khu mua sắm lớn.</p>\r\n<p style="text-align: justify;">Kh&aacute;ch sạn H&agrave; Nội Nikko được Sở du lịch quốc gia Việt Nam xếp hạng l&agrave; một trong 10 kh&aacute;ch sạn h&agrave;ng đầu tại Việt Nam.</p>\r\n<h1 style="text-align: justify;"><span style="font-size: large;"><strong>Th&aacute;ch thức</strong></span></h1>\r\n<p style="text-align: justify;">Nikko muốn gi&agrave;nh được sự ch&uacute; &yacute; v&agrave; trung  th&agrave;nh của những kh&aacute;ch h&agrave;ng kh&oacute; t&iacute;nh nhất với mục ti&ecirc;u cuối c&ugrave;ng l&agrave; tăng  cao doanh thu của kh&aacute;ch sạn. Nhưng Nikko đ&atilde; vấp phải nhiều kh&oacute; khăn, bởi  t&ugrave;y từng đối tượng kh&aacute;ch h&agrave;ng kh&aacute;c nhau, họ lại c&oacute; những y&ecirc;u cầu kh&aacute;c  nhau khi nghỉ tại kh&aacute;ch sạn. Lượng kh&aacute;ch thường xuy&ecirc;n h&agrave;ng năm của Nikko  l&ecirc;n tới h&agrave;ng chục ngh&igrave;n người n&ecirc;n việc quản l&yacute; v&agrave; chăm s&oacute;c cho từng  kh&aacute;ch h&agrave;ng tốn k&eacute;m rất nhiều chi ph&iacute; m&agrave; vẫn kh&ocirc;ng thể tr&aacute;nh khỏi những  thiếu s&oacute;t.</p>\r\n<h1 style="text-align: justify;"><span style="font-size: large;"><strong>Giải ph&aacute;p c&ugrave;ng email marketing</strong></span></h1>\r\n<p style="text-align: justify;">Nikko nhận định để x&acirc;y dựng l&ograve;ng trung  th&agrave;nh th&igrave; trước nhất họ phải bắt đầu từ việc thu thập những th&ocirc;ng tin  ch&iacute;nh x&aacute;c. Th&ocirc;ng qua qu&aacute; tr&igrave;nh t&igrave;m kiếm những giải ph&aacute;p chăm s&oacute;c kh&aacute;ch  h&agrave;ng, kh&aacute;ch sạn được khuyến kh&iacute;ch đăng k&iacute; sử dụng <i>email</i> marketing  để thiết lập hệ thống gửi mail chăm s&oacute;c kh&aacute;ch h&agrave;ng v&agrave; th&ocirc;ng tin được  thu thập qua những mẫu form đăng k&yacute; từ website của họ. Dữ liệu n&agrave;y được  chia sẻ giữa c&aacute;c kh&aacute;ch sạn trong hệ thống của Nikko tr&ecirc;n to&agrave;n thế giới  để tổng hợp dữ liệu đầy đủ của từng kh&aacute;ch h&agrave;ng đ&atilde; từng chọn Nikko.</p>\r\n<p style="text-align: justify;">Khi dữ liệu đ&atilde; được ho&agrave;n chỉnh, chức năng ph&acirc;n loại kh&aacute;ch h&agrave;ng cho nh&agrave; cung cấp dịch vụ <u>email</u>  marketing sẽ được sử dụng để ph&acirc;n loại li&ecirc;n hệ kh&aacute;ch h&agrave;ng theo c&aacute;c  trường kh&aacute;c nhau như: giới t&iacute;nh, ng&agrave;nh nghề, chức vụ, độ tuổi, thu nhập,  th&oacute;i quen&hellip; Sự ph&acirc;n loại n&agrave;y sẽ gi&uacute;p cho việc chăm s&oacute;c kh&aacute;ch h&agrave;ng theo  từng đối tượng đạt được hiệu quả tối ưu nhất.</p>\r\n<h1 style="text-align: justify;"><span style="font-size: large;"><strong>Ph&acirc;n nh&oacute;m kh&aacute;ch h&agrave;ng</strong></span></h1>\r\n<p style="text-align: justify;">- Ph&acirc;n nh&oacute;m theo giới t&iacute;nh<br />\r\nGiới t&iacute;nh kh&aacute;ch h&agrave;ng được ph&acirc;n theo 2 trường: nam v&agrave; nữ để kh&aacute;ch sạn c&oacute;  thể đưa tới những th&ocirc;ng tin ph&ugrave; hợp cho từng đối tượng. V&iacute; dụ, nếu kh&aacute;ch  sạn c&oacute; chương tr&igrave;nh phục vụ đặc biệt d&agrave;nh cho nữ giới, khi gửi mail  th&ocirc;ng b&aacute;o, việc ph&acirc;n nh&oacute;m kh&aacute;ch h&agrave;ng sẽ gi&uacute;p Nikko tự động chỉ gửi th&ocirc;ng  b&aacute;o tới những kh&aacute;ch h&agrave;ng l&agrave; nữ.</p>\r\n<p style="text-align: justify;">- Ph&acirc;n nh&oacute;m theo vị tr&iacute; địa l&yacute;<br />\r\nKhi cần quảng c&aacute;o cho cơ sở vừa khai trương, chiến dịch sẽ sẽ đạt hiệu  quả cao khi sử dụng những dữ liệu đ&atilde; được ph&acirc;n loại theo vị tr&iacute; địa l&yacute;  của cơ sở mới đ&oacute;. Email c&aacute;c kh&aacute;ch h&agrave;ng của Nikko sẽ được ph&acirc;n loại dựa  tr&ecirc;n địa chỉ IP của email v&agrave; dữ liệu về c&aacute;c địa điểm kh&aacute;ch h&agrave;ng đ&atilde; nghỉ  tại đ&oacute; trong qu&aacute; khứ.</p>\r\n<p style="text-align: justify;">- Ph&acirc;n nh&oacute;m theo cấp độ th&agrave;nh vi&ecirc;n<br />\r\nNikko d&ugrave;ng cấp độ th&agrave;nh vi&ecirc;n để ph&aacute;t triển những dịch vụ độc quyền cho  những th&agrave;nh vi&ecirc;n l&agrave; kh&aacute;ch h&agrave;ng VIP của kh&aacute;ch sạn hoặc những chiến dịch  chăm s&oacute;c thường xuy&ecirc;n cho kh&aacute;ch h&agrave;ng của m&igrave;nh. Việc chăm s&oacute;c kh&aacute;ch h&agrave;ng  thường xuy&ecirc;n mỗi th&aacute;ng sẽ gi&uacute;p kh&aacute;ch sạn tạo ấn tượng tốt đẹp đối với  lượng kh&aacute;ch h&agrave;ng phổ th&ocirc;ng cũng như đối với nh&oacute;m kh&aacute;ch VIP của m&igrave;nh.</p>\r\n<h1 style="text-align: justify;"><span style="font-size: large;"><strong>Kết quả</strong></span></h1>\r\n<p style="text-align: justify;">Danh s&aacute;ch kh&aacute;ch h&agrave;ng trung th&agrave;nh của  kh&aacute;ch sạn Nikko thường xuy&ecirc;n được cải thiện. Trung b&igrave;nh cứ mỗi th&aacute;ng,  Nikko lại tăng th&ecirc;m 5% kh&aacute;ch h&agrave;ng trung th&agrave;nh so với th&aacute;ng trước.</p>', 1, 1, 'nikko', 17, '2013-08-25 20:45:48');
INSERT INTO `tbl_store` (`id`, `name`, `description`, `content`, `user_id`, `cat_id`, `tag`, `viewer`, `created`) VALUES
(9, 'Nội dung email nên chia sẻ mang tính chất xã hội', '<hr />', '<h1><span style="font-size: large;">Kết nối email marketing với social media</span></h1>\r\n<p>Nhằm khuyến kh&iacute;ch người nhận mở <i>email</i>, bạn cần x&atilde; hội h&oacute;a nội dung <u>email</u>  của bạn v&agrave; chia sẻ th&ocirc;ng điệp email marketing của bạn th&ocirc;ng qua mạng x&atilde;  hội m&agrave; kh&aacute;ch h&agrave;ng đang tham gia như facebook, twitter, Google Plus&hellip;  th&ecirc;m v&agrave;o đ&oacute; l&agrave; sự kết hợp với c&aacute;c phương tiện truyền th&ocirc;ng (social  media) để hướng kh&aacute;ch h&agrave;ng tiềm năng gh&eacute; thăm trang web của bạn. Đ&oacute; cũng  l&agrave; một &yacute; tưởng tốt để đưa c&aacute;c chiến dịch email marketing&nbsp; kết nối với  c&aacute;c mạng x&atilde; hội của bạn.</p>\r\n<p>V&agrave; theo chiều ngược lại, sử dụng c&aacute;c phương tiện truyền th&ocirc;ng để lấp  đầy &ldquo;khoảng trống&rdquo; giữa c&aacute;c chiến dịch email của bạn. Social media cung  cấp 1 nền tảng tuyệt vời để bạn tham gia, kết nối c&ugrave;ng bạn b&egrave; v&agrave; những  th&agrave;nh vi&ecirc;n kh&aacute;c trong c&ugrave;ng&nbsp; mạng x&atilde; hội với bạn.</p>\r\n<p>Một sự kết hợp tuyệt vời giữa email marketing v&agrave; social media sẽ tạo  ra một k&ecirc;nh b&aacute;n h&agrave;ng l&yacute; tưởng cho bạn. Khi đăng k&yacute; tham gia c&aacute;c mạng x&atilde;  hội lu&ocirc;n đi k&egrave;m email v&agrave; ngược lại, trong email của bạn lu&ocirc;n c&oacute; nội dung  v&agrave; link li&ecirc;n kết đến c&aacute;c mạng x&atilde; hội đang được ưu chuộng. Khi đ&oacute;, bạn  sẽ c&oacute; hai c&ocirc;ng cụ l&yacute; tưởng đễ giữ kh&aacute;ch h&agrave;ng v&agrave; tăng cơ hội quảng b&aacute;  thương hiệu của bạn.</p>\r\n<p>Ch&uacute;ng ta h&atilde;y c&ugrave;ng suy nghĩ rộng hơn, trung b&igrave;nh người sử dụng  facebook kết nối khoảng 230 phương tiện truyền th&ocirc;ng. V&agrave; &ldquo;như những lo&agrave;i  chim đổ về nơi cư tr&uacute; tr&aacute;nh r&eacute;t&rdquo; tại đ&oacute; người nhận email marketing v&agrave;  c&aacute;c phương tiện truyền th&ocirc;ng sẽ chia sẻ mối quan t&acirc;m, thương hiệu c&ocirc;ng  ty bạn với nhưng người bạn của họ. Điều đ&oacute; c&oacute; nghĩa l&agrave; một email c&oacute; nội  dung về chia sẻ x&atilde; hội sẽ c&oacute; khả năng tiếp cận kh&aacute;ch h&agrave;ng cao hơn k&iacute;ch  thước danh s&aacute;ch email b&igrave;nh thường cho ph&eacute;p.&nbsp; Nhờ c&aacute;c social media, email  marketing đ&atilde; ph&aacute;t triển từ một c&ocirc;ng cụ lưu trữ (như gi&aacute; trị truyền  thống vốn c&oacute; của n&oacute;) th&agrave;nh một k&ecirc;nh b&aacute;n h&agrave;ng tuyệt vời.</p>', 1, 1, 'email marketing', 21, '2013-08-25 20:47:32'),
(10, 'Gửi email marketing hiệu quả', 'Cách gửi email marketing hiệu quả (Tỉ lệ vào inbox cao):', '<p><b>1.Kh&ocirc;ng n&ecirc;n thu thập c&aacute;c địa chỉ email từ c&aacute;c website tr&ecirc;n internet,  kh&ocirc;ng sử dụng hoặc mua c&aacute;c địa chỉ email được rao b&aacute;n tr&ecirc;n mạng<br />\r\n<br />\r\n2.Kh&ocirc;ng viết hoa cho tất cả nội dung thư hoặc tr&ecirc;n d&ograve;ng ti&ecirc;u đề<br />\r\n<br />\r\n3.Kh&ocirc;ng sử dụng video, flash hoặc javascrip trong email<br />\r\n<br />\r\n4.Sử  dụng thuộc t&iacute;nh alt trong tất cả h&igrave;nh ảnh được sử dụng trong email.Thẻ  alt sẽ gi&uacute;p người đọc dễ d&agrave;ng nhận biết nội dung h&igrave;nh ảnh của bạn<br />\r\n<br />\r\n5.Kh&ocirc;ng n&ecirc;n để c&aacute;c từ như Miễn Ph&iacute;,Khuyến m&atilde;i, Giảm gi&aacute; hoặc số tiền tr&ecirc;n ti&ecirc;u đề thư<br />\r\n<br />\r\n6.Kh&ocirc;ng n&ecirc;n nh&uacute;ng c&aacute;c biểu mẫu v&agrave;o nội dung email<br />\r\n<br />\r\n7.Kh&ocirc;ng n&ecirc;n sử dụng font chữ m&agrave;u đỏ trong email.Kh&ocirc;ng sử dụng m&agrave;u chữ gần giống với m&agrave;u n&ecirc;n của email<br />\r\n<br />\r\n8.Kh&ocirc;ng sử dụng dấu chấm than (!) trong email<br />\r\n<br />\r\n9.Kh&ocirc;ng sử dụng h&igrave;nh ảnh qu&aacute; lớn v&agrave; kh&ocirc;ng qu&ecirc;n th&ecirc;m c&aacute;c thẻ alt v&agrave;o mỗi h&igrave;nh ảnh<br />\r\n<br />\r\n10.Kiểm tra c&aacute;c lỗi ch&iacute;nh tả trong email, lỗi ch&iacute;nh tả l&agrave; một chỉ số trong việc đ&aacute;nh gi&aacute; Spam<br />\r\n<br />\r\n11.N&ecirc;n d&ugrave;ng c&aacute;c c&ocirc;ng cụ kiểm tra mail sống chết trước mỗi chiến dịch, khi gửi đến những mail đ&atilde; kh&ocirc;ng c&ograve;n tồn tại Email <br />\r\ncủa bạn c&oacute; thể bị đ&aacute;nh dấu SPam v&agrave; thậm ch&iacute; IP sẽ bị đưa v&agrave;o Blacklist<br />\r\n<br />\r\n12.Kh&ocirc;ng n&ecirc;n gửi file đ&iacute;nh k&egrave;m v&agrave;o mail khi gửi email marketing, tốt nhất h&atilde;y đặt li&ecirc;n kết tải file v&agrave;o trong nội dung email</b><br />\r\n<br />\r\nNếu bạn l&agrave;m đ&uacute;ng c&aacute;c quy tắc tr&ecirc;n đ&acirc;y, chiến dịch email marketing của bạn sẽ đạt hiệu quả cao.</p>', 1, 1, 'email marketing', 16, '2013-08-25 20:50:28'),
(11, 'So sánh thiết kế BMW serie 4 và serie 3 coupe ', 'Mẫu xe mới serie 4 của BMW có thiết kế khá giống đàn anh, chỉ thực sự nổi bật ở nội thất.', '<p class="Normal">Ng&ocirc;n ngữ thiết kế mới của BMW &aacute;p dụng tr&ecirc;n serie 4  dường như chỉ tăng th&ecirc;m số đường g&acirc;n v&agrave; điều chỉnh một ch&uacute;t c&aacute;c chi tiết  nhỏ như đ&egrave;n, lưới tản nhiệt hay gương chiếu hậu. Khi so s&aacute;nh serie 4 v&agrave;  serie 3 coupe, sự kh&aacute;c biệt lớn nhất tạo cảm gi&aacute;c hứng th&uacute; lại đến từ  nội thất chứ kh&ocirc;ng phải ngoại thất.</p>\r\n<p class="Normal">Chiếc xe serie 3 sử dụng so s&aacute;nh trong phần h&igrave;nh ảnh  l&agrave; bản n&acirc;p cấp năm 2010, với h&igrave;nh d&aacute;ng tổng thể kh&ocirc;ng kh&aacute;c nhiều so với  serie 4.</p>\r\n<p><img width="490" height="270" border="1" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-4er-3er.jpg" alt="" /></p>\r\n<p class="Normal">Th&ecirc;m đường cong v&agrave; vuốt d&agrave;i kiểu tr&ugrave;m mui về phần đu&ocirc;i  n&ecirc;n serie 4 (tr&ecirc;n) nh&igrave;n c&oacute; phần mập mạp hơn. Phần h&igrave;nh ảnh chi tiết  dưới đ&acirc;y với serie 4 b&ecirc;n tr&aacute;i v&agrave; serie 3 coupe b&ecirc;n phải.</p>\r\n<p><img width="245" height="163" border="1" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops103.jpg" alt="" /><img width="245" height="163" border="1" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops930.jpg" alt="" /></p>\r\n<p class="Normal">Hốc đ&egrave;n sương m&ugrave;, đ&egrave;n pha v&agrave; lưới tản nhiệt thiết kế lại.</p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops83.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops73.jpg" alt="" /></p>\r\n<p align="left" class="Normal">Phần sau cũng được thiết kế lại sắc n&eacute;t hơn.</p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops43.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops33[1].jpg" alt="" /></p>\r\n<p align="left" class="Normal">Nếu kh&ocirc;ng phải l&agrave; t&iacute;n đồ của BMW th&igrave; rất kh&oacute; để ph&acirc;n biệt hai d&ograve;ng xe.</p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops423.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops413.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops403.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops393.jpg" alt="" /></p>\r\n<p align="left" class="Normal">Đ&egrave;n hậu với h&igrave;nh d&aacute;ng mới c&oacute; phần sắc sảo hơn.</p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops343.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops333.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops323.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops313.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops303.jpg" alt="" /></p>\r\n<p align="center"><img width="245" height="163" border="1" align="middle" src="http://vnexpress.net/Files/Subject/3b/be/57/b6/BMW-3vs4er-Carscoops293.jpg" alt="" /></p>\r\n<p align="left" class="Normal">Nội hất bắt mắt, thể thao hơn với bảng điều khiển mới, cộng với c&aacute;ch phối m&agrave;u gi&uacute;p xe bớt đơn điệu.</p>\r\n<p align="right" class="Normal"><strong>Đức Huy<br />\r\n</strong><em>Ảnh:</em><strong> Carscoops</strong></p>', 1, 1, 'BMW, oto', 6, '2013-06-23 15:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template`
--

CREATE TABLE IF NOT EXISTS `tbl_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `key` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `variables` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_template`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `subscribed` int(11) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `subscribed`, `activkey`, `create_at`, `lastvisit_at`, `superuser`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', 0, '9a24eff8c15a6a141ece27eb6947da0f', '2013-07-23 15:11:29', '2013-08-25 15:46:56', 1, 1),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', 0, '099f825543f7850cc038b90aaff39fac', '2013-07-23 15:11:29', '2013-08-30 00:31:47', 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
