<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<hr />

<?php echo StoreModule::t('Name')?>: <br />
<?php echo $form->textField($model,'name',array('class'=>'span5','maxlength'=>150)); ?> <br />
<table border="0" width="456" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<?php echo StoreModule::t('User')?>:
		</td>
		<td>
			<?php echo StoreModule::t('Category')?>:
		</td>
	</tr>
	<tr>
		<td>
			<?php 
				echo $form->dropdownlist($model,'user_id', CHtml::listData(User::model()->findAll() , 'id', 'username'),
					array(
			        		'class' => 'my-drop-down',	
			            	'options' => array(
			            		$model->id => array(
			                	'selected' => "selected"
			            	)
			       		)
			    	)
			    );
			?>
		</td>
		<td>
			<?php 
				echo $form->dropdownlist($model,'cat_id', CHtml::listData(Categories::model()->findAll() , 'id', 'name'),
					array(
			        		'class' => 'my-drop-down',	
			            	'options' => array(
			            		$model->id => array(
			                	'selected' => "selected"
			            	)
			       		)
			    	)
			    );
			?>
		</td>
	</tr>
</table>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>StoreModule::t('Search'),
	)); ?>
</div>

<?php $this->endWidget(); ?>
