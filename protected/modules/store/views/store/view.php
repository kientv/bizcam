<?php
$this->breadcrumbs=array(
	StoreModule::t('Stores')=>array('index'),
	$model->name,
);

if (Yii::app()->user->checkAccess('admin')) {
	$this->menu=array(
		array('label'=>StoreModule::t('List Store'),'url'=>array('index')),
		array('label'=>StoreModule::t('Create Store'),'url'=>array('create')),
		array('label'=>StoreModule::t('Update Store'),'url'=>array('update','id'=>$model->id)),
		array('label'=>StoreModule::t('Delete Store'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>StoreModule::t('Manage Store'),'url'=>array('admin')),
	);
} else {
	$criteria = new CDbCriteria;  
	$criteria->limit = 20;
	$results = Store::model()->findAll($criteria);
	foreach($results as $result) { 
		$menu[] = array('label'=>$result->name, 'url'=>array('/store/store/view/', 'id'=>$result->id));
	}
	$this->menu = $menu;
}
$this->pageTitle = Yii::app()->name . ' ' . $model->name;
?>

<h5><?php echo $model->name; ?></h5>
<p align="justify">
	<b>[<?php echo $model->categories->name?>]</b>  
	<i><?php echo $model->description?></i>
</p>
<p align="justify">
	<?php echo $model->content?>
</p>
<!-- 
<hr />
<?php echo $model->tag?>
-->
<hr />
<i>Created by <b><?php echo $model->user->username?></b> at <?php echo $model->created?></i>
 (<?php echo $model->viewer?>)<br />
