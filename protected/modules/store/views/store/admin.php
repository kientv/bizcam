<?php
$this->breadcrumbs=array(
	StoreModule::t('Stores')=>array('index'),
	StoreModule::t('Manage'),
);

$this->menu=array(
	array('label'=>StoreModule::t('List Store'),'url'=>array('index')),
	array('label'=>StoreModule::t('Create Store'),'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('store-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo StoreModule::t('Manage Stores') ?></h1>

<?php echo CHtml::link(StoreModule::t('Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'store-grid',
	'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name' => 'name',
			'header' => StoreModule::t('Name'),
			'value' => $data->name,	
		),
		array(
			'name' => 'user_id',
			'header' => StoreModule::t('User'),
			'value' => $data->user->username,	
		),
		array(
			'name' => 'cat_id',
			'header' => StoreModule::t('Category'),
			'value' => $data->categories->name,	
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>