<?php
$this->breadcrumbs=array(
	StoreModule::t('Stores')=>array('index'),
	StoreModule::t('Create'),
);

$this->menu=array(
	array('label'=>StoreModule::t('List Store'),'url'=>array('index')),
	array('label'=>StoreModule::t('Manage Store'),'url'=>array('admin')),
);
?>

<h1><?php echo StoreModule::t('Create Store') ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>