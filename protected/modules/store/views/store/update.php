<?php
$this->breadcrumbs=array(
	StoreModule::t('Stores')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	StoreModule::t('Update'),
);

$this->menu=array(
	array('label'=>StoreModule::t('List Store'),'url'=>array('index')),
	array('label'=>StoreModule::t('Create Store'),'url'=>array('create')),
	array('label'=>StoreModule::t('View Store'),'url'=>array('view','id'=>$model->id)),
	array('label'=>StoreModule::t('Manage Store'),'url'=>array('admin')),
);
?>

<h1><?php echo StoreModule::t('Edit') . ': ' .$model->name; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>