<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'store-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo StoreModule::t('Fields with <span class="required">*</span> are required.')?></p>

	<?php echo $form->errorSummary($model); ?>
	<?php echo StoreModule::t('Name')?>: (*) <br />
	<?php echo $form->textField($model,'name',array('class'=>'span6','maxlength'=>150)); ?> <br />
	<?php echo StoreModule::t('Description')?>: (*) <br />
	<?php echo $form->textArea($model,'description',array('rows'=>3, 'cols'=>50, 'class'=>'span6')); ?> <br />
	<?php echo StoreModule::t('Content')?>: (*) <br />
	<?php $this->widget('application.extensions.fckeditor.FCKEditorWidget',array(
	    "model"=>$model,                # Data-Model
	    "attribute"=>'content',         # Attribute in the Data-Model
	    "height"=>'400px',
	    "width"=>'100%',
	    "toolbarSet"=>'Custom',          # EXISTING(!) Toolbar (see: fckeditor.js)
	    "fckeditor"=>Yii::app()->basePath."/../fckeditor/fckeditor.php",
	                                    # Path to fckeditor.php
	    "fckBasePath"=>Yii::app()->baseUrl."/fckeditor/",
	                                    # Realtive Path to the Editor (from Web-Root)
	    "config" => array("EditorAreaCSS"=>Yii::app()->baseUrl.'/css/main.css',),
	                                    # Additional Parameter (Can't configure a Toolbar dynamicly)
	) ); ?>
	<table border="0" width="100%">
		<tr>
			<td>
				<?php echo StoreModule::t('Editor')?>: (*)
			</td>
			<td>
				<?php echo StoreModule::t('Categories')?>: (*) <br />
			</td>
		</tr>
		<tr>
			<td>
				<?php echo
					$form->dropDownList($model, 'user_id', CHtml::listData(User::model()->findAll(), 'id', 'username'), 
			            array(
			                'class' => 'my-drop-down',
			                'options' => array(
			                    4 => array(
			                        'selected' => "selected"
			                    )
			                )
			            )
			      );
			    ?>			
			</td>
			<td>
				<?php echo
					$form->dropDownList($model, 'cat_id', CHtml::listData(Categories::model()->findAll(), 'id', 'name'), 
			            array(
			                'class' => 'my-drop-down',
			                'options' => array(
			                    4 => array(
			                        'selected' => "selected"
			                    )
			                )
			            )
			      );
			    ?>
			</td>
		</tr>
	</table>
	<br />
	<?php echo StoreModule::t('Tag')?>: <br />
	<?php echo $form->textField($model,'tag',array('class'=>'span6','maxlength'=>200)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? StoreModule::t('Create') : StoreModule::t('Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
