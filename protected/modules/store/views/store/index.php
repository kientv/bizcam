<?php
/**
 * display breadcrumbs
 */
$this->breadcrumbs=array(
	StoreModule::t('Stores'),//$this->pageTitle,
);

if (Yii::app()->user->checkAccess('admin')) {
	$this->menu=array(
		array('label'=>StoreModule::t('Create Store'),'url'=>array('create')),
		array('label'=>StoreModule::t('Manage Store'),'url'=>array('admin')),
	);
} else {
	$results = Categories::model()->findAll();
	foreach($results as $result) { 
		$menu[] = array('label'=>$result->name, 'url'=>array('/store/store/', 'cid'=>$result->id));
	}
	$this->menu = $menu;
}
$this->pageTitle = Yii::app()->name . ' ' . Yii::t('app', 'News');
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
	'pagerCssClass' => 'pagination',
	'pager'=>array(
		'class'=>'CLinkPager',
        'header'         => '&nbsp;',
		'cssFile'=>false,
		'maxButtonCount'=>25,
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'firstPageCssClass'=>'previous',
		'lastPageCssClass'=>'next',
        'firstPageLabel' => '<<',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'lastPageLabel'  => '>>',
    ),
    'template'=>'{items}{pager}',
    'columns'=>array(
        array(
        	'name'  => 'Name',
        	'header' => StoreModule::t('Name'),
        	'value' => 'CHtml::link($data->name,Yii::app()->createUrl("store/store/view",array("id"=>$data->primaryKey)))',
        	'type'  => 'raw',
    	),
    ),
)); ?>