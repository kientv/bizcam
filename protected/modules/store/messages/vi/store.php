<?php
/**
* (c) kientranvan@gmail.com,
* (c) Trần Văn Kiên
*/
return array(
	'Stores' => 'Email marketing',
	'Create Store' => 'Viết bài',
	'Manage Store' => 'Quản lý',
	'Manage Stores' => 'Quản lý',
	'Manage' => 'Quản lý',
	'Advanced Search' => 'Tìm kiếm',
	'Search' => 'Tìm kiếm',
	'User' => 'Người soạn',
	'Category' => 'Chuyên mục',
	'Name' => 'Tiêu đề',
	'Content' => 'Nội dung',
	'Editor' => 'Người soạn',
	'Edit' => 'Cập nhật',
	'Categories' => 'Chuyên mục',
	'Tag' => 'Tag',
	'Create' => 'Soạn bài',
	'Update' => 'Cập nhật',
	'Description' => 'Mô tả',
	'Save' => 'Cập nhật',
	'Fields with <span class="required">*</span> are required.' => 'Thông tin gắn * là bắt buộc',
	'List Store' => 'Danh sách bài',
	'Create Store' => 'Soạn bài',
	'Update Store' => 'Cập nhật bài',
	'View Store' => 'Xem trước',
	'Delete Store' => 'Xóa bài',
	'Manage Store' => 'Quản lý bài',
	'Are you sure you want to delete this item?' => 'Bạn thật sự muốn xóa bài này?',
);